﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq;
using log4net;
using System.Net;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;
using Timer = System.Threading.Timer;
using System.IO;
using System.Drawing.Imaging;

namespace ZKPushSimulator
{
    class ZKPushClient
    {
        private string _Server = string.Empty;
        private string _Ns = string.Empty;
        private string _AuthType = string.Empty;
        private string serverVersion = "2.0.22";
        private string serverName = "ADMS";
        private string pushVersion = "2.0.22";
        private int errorDelay = 30;
        private string transTimes = "00:0014:00";
        private int requestDelay = 1;
        private int transInterval = 1;
        private int realTime = 1;
        private string transTables = "User Transaction Facev7 templatev10";
        private int timeZone = -3;
        private int timeoutSec = 5;
        Timer tmrProcess;
        Timer tmrEvents;
        private List<Person> personList = new List<Person>();
        private List<Face> faceList = new List<Face>();
        private List<Finger> fingerList = new List<Finger>();
        private List<Face> newFaceList = new List<Face>();
        private List<Finger> newFingerList = new List<Finger>();
        private List<Record> recordList = new List<Record>();
        private List<Action> actionList = new List<Action>();
        private List<Palm> palmList = new List<Palm>();
        private List<Palm> newPalmList = new List<Palm>();
        private bool autoServerFunOn = false;
        private bool saveImageFace = false;
        private int _cmdId = 1;


        enum EStatus
        {
            WAIT,
            CDATE,
            REGISTER,
            PUSH,
            SENDDATA,
            DEVICERETURNRESULT,
            GETREQUEST,
            RTDATE,
            DEVICECMD,
            QUERYDATA,
        }

        private EStatus deviceStatus = EStatus.WAIT;
        private bool isRegistry = false;
        private string rtdataType = string.Empty;
        private string rtdataContent = string.Empty;
        private string deviceCmdContent = string.Empty;
        private string queryTableName = string.Empty;
        private string queryFieldDesc = string.Empty;
        private string queryFilter = string.Empty;
        private string queryResultUrl = string.Empty;
        private StringBuilder queryResultData;
        private ILog log;

        public struct queryResultControlStruct
        {
            public int ItensCount;
            public int Index;
            public string Table;
            public string data;
        };

        queryResultControlStruct queryResultControl = new queryResultControlStruct();

        public ZKPushClient(Device device, ILog logger)
        {
            _Server = device.ServerUrl;
            _Ns = device.Ns;
            this.personList = device.PersonList == null ? new List<Person>() : device.PersonList;
            this.faceList = device.FaceList == null ? new List<Face>() : device.FaceList;
            this.fingerList = device.FingerList == null ? new List<Finger>() : device.FingerList;
            this.recordList = device.RecordEventList == null ? new List<Record>() : device.RecordEventList;
            this.actionList = device.ActionEventList == null ? new List<Action>() : device.ActionEventList;
            this.newFaceList = device.NewFaceList == null ? new List<Face>() : device.NewFaceList;
            this.newFingerList = device.NewFingerList == null ? new List<Finger>() : device.NewFingerList;
            this.palmList = device.PalmList == null ? new List<Palm>() : device.PalmList;
            this.newPalmList = device.NewPalmList == null ? new List<Palm>() : device.NewPalmList;
            this._AuthType = device.AuthType == null ? "CARD" : device.AuthType;
            this.saveImageFace = device.SaveImageFace;
            tmrProcess = new Timer(Proccess, null, Timeout.Infinite, Timeout.Infinite);
            tmrEvents = new Timer(EventsRun, null, Timeout.Infinite, Timeout.Infinite);
            log = logger;
        }

        private void SaveImage(string base64Image,string fileName)
        {
            byte[] bytes = Convert.FromBase64String(base64Image);
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                Image image;
                image = Image.FromStream(ms);
                image.Save(fileName, ImageFormat.Jpeg);
                image.Dispose();
            }
        }

        public void Start()
        {
            tmrProcess.Change(timeoutSec * 1000, timeoutSec * 1000);
            tmrEvents.Change(1000, 1000);
        }

        public void Stop()
        {
            tmrEvents.Change(Timeout.Infinite, Timeout.Infinite);
            tmrProcess.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private void EventsRun(object state)
        {
            tmrEvents.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                if (isRegistry)
                {
                    #region Records

                    if(recordList?.Count > 0)
                    {
                        recordList.ForEach(x => x.CounterRepeat++);
                        var recListSend = recordList?.Where(x => x.CounterRepeat > x.RepeatInSec).ToList();
                        SendRecords(recListSend);
                        recordList?.Where(r => r.CounterRepeat > r.RepeatInSec).ToList().ForEach(x => x.CounterRepeat = 0);
                    }

                    #endregion

                    #region Actions
                    if(actionList?.Count > 0)
                    {
                        actionList.ForEach(x => x.CounterRepeat++);
                        var actListSend = actionList?.Where(x => x.CounterRepeat > x.RepeatInSec).ToList();
                        SendActions(actListSend);
                        actionList?.Where(r => r.CounterRepeat > r.RepeatInSec).ToList().ForEach(x => x.CounterRepeat = 0);
                    }

                    #endregion

                    #region NewFinger

                    if(newFingerList?.Count > 0)
                    {
                        newFingerList.ForEach(x => x.CounterRepeat++);
                        var newFingerListSend = newFingerList?.Where(x => x.CounterRepeat > x.RepeatInSec).ToList();
                        SendNewsFingers(newFingerListSend);
                        newFingerList?.Where(r => r.CounterRepeat > r.RepeatInSec).ToList().ForEach(x => x.CounterRepeat = 0);
                    }


                    #endregion

                    #region NewFace

                    if(newFaceList?.Count > 0)
                    {
                        newFaceList.ForEach(x => x.CounterRepeat++);
                        var newFaceListSend = newFaceList?.Where(x => x.CounterRepeat > x.RepeatInSec).ToList();
                        SendNewsFaces(newFaceListSend);
                        newFaceList?.Where(r => r.CounterRepeat > r.RepeatInSec).ToList().ForEach(x => x.CounterRepeat = 0);
                    }

                    #endregion

                    #region NewPalm

                    if(newPalmList?.Count > 0)
                    {
                        newPalmList.ForEach(x => x.CounterRepeat++);
                        var newPalmListSend = newPalmList?.Where(x => x.CounterRepeat > x.RepeatInSec).ToList();
                        SendNewsPalms(newPalmListSend);
                        newPalmList?.Where(r => r.CounterRepeat > r.RepeatInSec).ToList().ForEach(x => x.CounterRepeat = 0);
                    }

                    #endregion

                    if (autoServerFunOn && recordList?.Count > 0)
                    {
                        SendInquireAccess(recordList[0], this._AuthType);
                    }

                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("EventsRun: {0}", ex.Message);
            }
            finally
            {
                tmrEvents.Change(1000, 1000);
            }
        }

        private void Proccess(object state)
        {
            tmrProcess.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                switch (deviceStatus)
                {
                    case EStatus.WAIT:
                        deviceStatus = EStatus.CDATE;
                        break;
                    case EStatus.CDATE:
                        SendRegister();
                        break;
                    case EStatus.REGISTER:
                        SendRegisterPost();
                        break;
                    case EStatus.PUSH:
                        SendPushPost();
                        break;
                    case EStatus.GETREQUEST:
                        SendGetRequest();
                        break;
                    case EStatus.RTDATE:
                        SendRtData();
                        break;
                    case EStatus.DEVICECMD:
                        SendDeviceCmd();
                        break;
                    case EStatus.QUERYDATA:
                        SendQueryData();
                        break;
                    case EStatus.SENDDATA:

                        break;
                    case EStatus.DEVICERETURNRESULT:

                        break;
                    default:
                        break;
                }
            }
            finally
            {
                if (isRegistry)
                {
                    tmrProcess.Change(requestDelay * 1000, requestDelay * 1000);
                }
                else
                {
                    tmrProcess.Change(timeoutSec * 1000, timeoutSec * 1000);
                }
            }
        }

        private async Task SendRecords(List<Record> records)
        {
            try
            {
                if (records == null || records?.Count == 0) return;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    // time=2020-07-27 12:11:57	pin=1	cardno=0	eventaddr=1	event=3	inoutstatus=0	verifytype=15	index=372	sitecode=0	linkid=0	maskflag=0	temperature=36.6
                    foreach (var r in records)
                    {
                        strData.AppendFormat("time={0:yyyy-MM-dd HH:mm:ss}\tpin={1}\tcardno={2}\teventaddr={3}\tevent={4}\tinoutstatus={5}\tverifytype={6}\tindex={7}\tsitecode={8}\tlinkid={9}\tmaskflag={10}\ttemperature={11}\t\r\n", 
                            DateTime.Now, r.Pin, r.CardNo, r.EventAddr, r.Event, r.InoutStatus, r.VerifyType, r.Index, r.SiteCode, r.LinkId, r.MaskFlag, r.Temperature.ToString().Replace(',', '.'));
                    }

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&table=rtlog", _Ns), data);

                    Console.WriteLine("SendRecord: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendRecord Err.: " + ex.Message);
            }
        }

        private async Task SendInquireAccess(Record r, string authType)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    // time=2019-12-10 19:20:16	pin=4	cardno=0	eventaddr=1	event=0	inoutstatus=1	verifytype=0	index=271	sitecode=0	linkid=0
                    strData.AppendFormat("time={0:yyyy-MM-dd HH:mm:ss}\tpin={1}\tcardno={2}\teventaddr={3}\tevent={4}\tinoutstatus={5}\tverifytype={6}\tindex={7}\tsitecode={8}\tlinkid={9}\r\n",
                            DateTime.Now, r.Pin, r.CardNo, r.EventAddr, r.Event, r.InoutStatus, r.VerifyType, r.Index, r.SiteCode, r.LinkId);

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&AuthType={1}", _Ns, authType), data);

                    Console.WriteLine("SendInquireAccess: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendInquireAccess Err.: " + ex.Message);
            }
        }

        private async Task SendActions(List<Action> actions)
        {
            try
            {
                if (actions == null || actions?.Count == 0) return;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    //time=2019-12-10 20:24:59	sensor=00	relay=01	alarm=0200000000000000	door=01
                    foreach (var r in actions)
                    {
                        strData.AppendFormat("time={0:yyyy-MM-dd HH:mm:ss}\tsensor={1}\trelay={2}\talarm={3}\tdoor={4}\r\n",
                            DateTime.Now, r.Sensor, r.Relay, r.Alarm, r.Door);
                    }

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&table=rtstate", _Ns), data);

                    Console.WriteLine("SendActions: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendActions Err.: " + ex.Message);
            }
        }

        private async Task SendNewsFingers(List<Finger> fingers)
        {
            try
            {
                if (fingers == null || fingers?.Count == 0) return;
                
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    // biodata pin=3	no=3	index=0	valid=1	duress=0	type=1	majorver=12	minorver=0	format=0	tmp=...
                    foreach (var f in fingers)
                    {
                        strData.AppendFormat("biodata pin={0}\tno={1}\tindex={2}\tvalid={3}\tduress={4}\ttype={5}\tmajorver={6}\tminorver={7}\tformat={8}\ttmp={9}\r\n",
                            f.Pin, f.No, f.Index, f.Valid, f.Duress, f.Type, f.Majorver, f.Minorver, f.Format, f.Tmp);
                    }

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&table=tabledata&tablename=biodata", _Ns), data);

                    Console.WriteLine("SendNewsFingers: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendNewsFingers Err.: " + ex.Message);
            }
        }

        private async Task SendNewsPalms(List<Palm> palms)
        {
            try
            {
                if (palms == null || palms?.Count == 0) return;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    int index = 0;
                    int count = 0;
                    foreach (var p in palms)
                    {
                        foreach (var t in p.TmpLst)
                        {
                            strData.AppendFormat("biodata pin={0}\tno={1}\tindex={2}\tvalid={3}\tduress={4}\ttype={5}\tmajorver={6}\tminorver={7}\tformat={8}\ttmp={9}\r\n",
                                p.Pin, p.No, index++, p.Valid, p.Duress, p.Type, p.Majorver, p.Minorver, p.Format, t);
                            count++;
                        }
                    }

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    // POST /iclock/querydata?SN=CKJF202260257&type=tabledata&cmdid=4&tablename=biodata&count=10&packcnt=1&packidx=1 HTTP/1.1
                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&table=tabledata&cmdid={1}&tablename=biodata&count={2}&packcnt=1&packidx=1", _Ns, _cmdId++, count), data);

                    Console.WriteLine("SendNewsPalms: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendNewsPalms Err.: " + ex.Message);
            }
        }

        private async Task SendNewsFaces(List<Face> faces)
        {
            try
            {
                if (faces == null || faces?.Count == 0) return;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    StringBuilder strData = new StringBuilder();

                    //"biophoto pin={0}\tfilename={1}\ttype={2}\tsize={3}\tcontent={4}\r\n"
                    foreach (var f in faces)
                    {
                        strData.AppendFormat("biophoto pin={0}\tfilename={1}\ttype={2}\tsize={3}\tcontent={4}\r\n",
                            f.PIN, "pin" + f.PIN + "jpg", f.Type, f.Size, f.Content);
                    }

                    var data = new StringContent(strData.ToString(), Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/cdata?SN={0}&table=tabledata&tablename=biophoto", _Ns), data);

                    Console.WriteLine("SendNewsFaces: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendNewsFaces Err.: " + ex.Message);
            }
        }

        private async Task SendRegister()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();
                    HttpResponseMessage response = await client.GetAsync(
                        string.Format("iclock/cdata?SN={0}&options=all&pushver={1}&DeviceType={2}", _Ns, pushVersion, "Push"));

                    Console.WriteLine("SendRegister: " + response.StatusCode);
                    //Console.WriteLine(response.Content);

                    if(response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if(isRegistry == false)
                        {
                            deviceStatus = EStatus.REGISTER;
                            isRegistry = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendRegister Err.: " + ex.Message);
            }
        }

        private async Task SendRegisterPost()
        {
            string postData = "~IsOnlyRFMachine,~MaxUserCount,~MaxAttLogCount,~MaxUserFingerCount,IPAddress,NetMask,GATEIPAddress,~ZKFPVersion,IclockSvrFun,OverallAntiFunOn,~REXInputFunOn,~CardFormatFunOn,~SupAuthrizeFunOn,~ReaderCFGFunOn,~ReaderLinkageFunOn,~RelayStateFunOn,~Ext485ReaderFunOn,~TimeAPBFunOn,~CtlAllRelayFunOn,~LossCardFunOn,SimpleEventType,VerifyStyles,EventTypes,DisableUserFunOn,DeleteAndFunOn,LogIDFunOn,DateFmtFunOn,DelAllLossCardFunOn,AutoClearDay,FirstDelayDay,DelayDay,StopAllVerify,FvFunOn,FaceFunOn,FingerFunOn,CameraOpen,IsSupportNFC,~MaxFingerCount,~DSTF,Reader1IOState,MachineTZFunOn,BioPhotoFun,BioDataFun,VisilightFun,~MaxBioPhotoCount";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    var data = new StringContent(postData, Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/registry?SN={0}", _Ns), data);

                    Console.WriteLine("SendRegisterPost: " + response.StatusCode);
                    //Console.WriteLine(response.Content);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        deviceStatus = EStatus.PUSH;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendRegisterPost Err.: " + ex.Message);
            }
        }

        // solicita ao servidor o envio de seus parâmetros
        private async Task SendPushPost()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    var data = new StringContent("", Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("iclock/push?SN={0}", _Ns), data);

                    Console.WriteLine("SendPushPost: " + response.StatusCode);
                    

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);

                        tmrProcess.Change(requestDelay * 1000, requestDelay * 1000);
                        deviceStatus = EStatus.GETREQUEST;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendPushPost Err.: " + ex.Message);
            }
        }

        private async Task SendGetRequest()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();
                    HttpResponseMessage response = await client.GetAsync(
                        string.Format("iclock/getrequest?SN={0}", _Ns));

                    Console.WriteLine("SendGetRequest: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Content: " + result);
                        ProcessRequest(result);
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendGetRequest Err.: " + ex.Message);
            }
        }

        private async Task SendRtData()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("Data", rtdataContent);
                    HttpResponseMessage response = await client.GetAsync(
                        string.Format("iclock/rtdata?SN={0}&type={1}", _Ns, rtdataType));

                    Console.WriteLine("SendRtData: " + response.StatusCode);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Content: " + result);
                        // Content: DateTime=657555562,ServerTZ=0
                        deviceStatus = EStatus.GETREQUEST;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendRtData Err.: " + ex.Message);
            }
        }

        private async Task SendDeviceCmd()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();

                    var data = new StringContent(deviceCmdContent, Encoding.UTF8, "application/push");

                    HttpResponseMessage response = await client.PostAsync(
                        string.Format("/iclock/devicecmd?SN={0}", _Ns), data);

                    Console.WriteLine("SendDeviceCmd: " + response.StatusCode);


                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);

                        deviceStatus = EStatus.GETREQUEST;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendDeviceCmd Err.: " + ex.Message);
            }
        }

        private void QueryResultControlReset()
        {
            queryResultControl = new queryResultControlStruct();
        }

        private async Task SendQueryData()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = false;
                    client.BaseAddress = new Uri(_Server);
                    client.DefaultRequestHeaders.Accept.Clear();
                    
                    var data = new StringContent(queryResultData.ToString(), Encoding.UTF8, "application/push");
                    HttpResponseMessage response = await client.PostAsync(queryResultUrl, data);

                    log.Info(queryResultData.ToString());

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        Console.WriteLine("Status do Servidor: " + result);

                        if(queryResultControl.Index >= queryResultControl.ItensCount)
                        {
                            QueryResultControlReset();
                            deviceStatus = EStatus.GETREQUEST;
                        }
                        else
                        {
                            ProcessRequest(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("SendQueryData Err.: " + ex.Message);
            }
        }

        private void ProcessRequest(string data)
        {
            if (!data.StartsWith("OK"))
            {
                log.Info(data);

                ProcDateTime(data);
                ProcAddUser(data);
                ProcRemoveUser(data);
                ProcAddBioPhoto(data);
                ProcRemoveBioPhoto(data);
                ProcRemovetemplatev10(data);
                ProcAddBioData(data);
                ProcRemoveBioData(data);
                ProcQuery(data);
                ProcControlDevice(data);
                ProcAutoServerFun(data);
                ProcAddTemplatev10(data);
            }
            else
            {
                ProcQuery(data);
            }
        }

        private void ProcAutoServerFun(string data)
        {
            int pos = data.IndexOf(":SET OPTIONS AutoServerFunOn");
            if (pos > 0)
            {
                var val = data.Substring(pos + 29);
                autoServerFunOn = int.Parse(val) == 1;
                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=SET OPTIONS", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcControlDevice(string data)
        {
            int pos = data.IndexOf(":CONTROL DEVICE");
            if (pos > 0)
            {
                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=CONTROL DEVICE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }

        private void ProcDateTime(string data)
        {
            int pos = data.IndexOf(":SET OPTIONS DateTime");
            if (pos > 0)
            {
                string address = data.Substring(0, pos);
                rtdataType = "time";
                rtdataContent = "DateTime=583050990,ServerTZ=+0800";
                deviceStatus = EStatus.RTDATE;
            }
        }
        private void ProcAddUser(string data)
        {
            int pos = data.IndexOf(":DATA UPDATE user");
            if (pos > 0)
            {
                var person = new Person(data);

                if (person != null)
                {
                    if (personList.Exists(x => x.Pin == person.Pin))
                        personList.RemoveAll(x => x.Pin == person.Pin);

                    personList.Add(person);
                }

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA UPDATE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcRemoveUser(string data)
        {
            int pos = data.IndexOf(":DATA DELETE user");
            if (pos > 0)
            {
                var person = new Person(data);
                if (person != null)
                {
                    personList.RemoveAll(x => x.Pin == person.Pin);
                }

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA DELETE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcAddBioPhoto(string data)
        {
            int pos = data.IndexOf(":DATA UPDATE biophoto");
            if (pos > 0)
            {
                var face = new Face(data);

                if (face != null)
                {
                    if (faceList.Exists(x => x.PIN == face.PIN))
                        faceList.RemoveAll(x => x.PIN == face.PIN);

                    faceList.Add(face);

                    if (saveImageFace)
                    {

                        SaveImage(face.Content, face.PIN.ToString() + ".jpg");
                    }
                }

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA UPDATE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }

        private void ProcAddTemplatev10(string data)
        {
            int pos = data.IndexOf(":DATA UPDATE templatev10");
            if (pos > 0)
            {
                //TODO: Update code aqui... 

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA UPDATE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcRemoveBioPhoto(string data)
        {
            int pos = data.IndexOf(":DATA DELETE biophoto");
            if (pos > 0)
            {
                var face = new Face(data);
                if(face != null)
                {
                    faceList.RemoveAll(x => x.PIN == face.PIN);
                }

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA DELETE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }

        private void ProcRemovetemplatev10(string data)
        {
            int pos = data.IndexOf(":DATA DELETE templatev10");
            if (pos > 0)
            {
                //var face = new Face(data);
                //if (face != null)
                //{
                //    faceList.RemoveAll(x => x.PIN == face.PIN);
                //}

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA DELETE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcAddBioData(string data)
        {
            int pos = data.IndexOf(":DATA UPDATE biodata");
            if (pos > 0)
            {
                var finger = new Finger(data);
                if (finger != null)
                {
                    if (fingerList.Exists(x => x.Pin == finger.Pin))
                        fingerList.RemoveAll(x => x.Pin == finger.Pin);

                    fingerList.Add(finger);
                }
                    
                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA UPDATE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }
        private void ProcRemoveBioData(string data)
        {
            int pos = data.IndexOf(":DATA DELETE biodata");
            if (pos > 0)
            {
                var finger = new Finger(data);
                if(finger != null)
                    fingerList.RemoveAll(x => x.Pin == finger.Pin && x.Index == finger.Index);

                var cmdId = data.Substring(0, pos).Split(':');
                deviceCmdContent = string.Format("ID={0}&Return=0&CMD=DATA DELETE", cmdId[1]);
                deviceStatus = EStatus.DEVICECMD;
            }
        }

        private void ProcQuery(string data)
        {
            decimal pinFilter = 0;
            string endDataStr = "\n"; // "\r\n"; Isso parece esta provocando falha para colerar (Obter pessoa total)

            if(data.Equals("OK") && queryResultControl.Index < queryResultControl.ItensCount)
            {
                data = queryResultControl.data;
            }

            int pos = data.IndexOf(":DATA QUERY");
            if(pos > 0)
            {
                queryResultControl.data = data;

                var cmdId = data.Substring(0, pos).Split(':');
                var param = data.Substring(pos + 12).Split(',');

                if(param.Length > 0)
                    queryTableName = param[0];
                if (param.Length > 1)
                    queryFieldDesc = param[1];
                if (param.Length > 2)
                    queryFilter = param[2];

                queryResultData = new StringBuilder();

                if (queryFilter.StartsWith("filter=Pin="))
                {
                    decimal.TryParse(queryFilter.Substring(11).Replace("\r\n", ""), out pinFilter);
                }

                if (queryTableName.EndsWith("=user"))
                {
                    var result = pinFilter == 0 ? personList : personList.Where(x => x.Pin == pinFilter).ToList();
                    if(result != null && result.Count > 0)
                    {
                        queryResultControl.ItensCount = result.Count;
                        queryResultControl.Index++;
                        queryResultControl.Table = "user";

                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=user&count={2}&packcnt={3}&packidx={4}",
                            _Ns, cmdId[1], 1, queryResultControl.ItensCount, queryResultControl.Index);

                        var p = result[queryResultControl.Index - 1];

                        queryResultData.AppendFormat("user uid={0}\tcardno={1}\tpin={2}\tgroup={3}\tname={4}\tprivilege={5}\tpassword={6}" + endDataStr,
                            queryResultControl.Index, p.CardNo, p.Pin, p.Group, p.Name, p.Privilege, p.Password);
                    }
                    else
                    {
                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=user&count={2}&packcnt={3}&packidx={4}",
                            _Ns, cmdId[1], 1, 0, 0);
                    }
                }
                else if (queryTableName.EndsWith("=biodata"))
                {
                    var result = pinFilter == 0 ? fingerList : fingerList.Where(x => x.Pin == pinFilter).ToList();
                    if (result != null && result.Count > 0)
                    {
                        queryResultControl.ItensCount = result.Count;
                        queryResultControl.Index++;
                        queryResultControl.Table = "biodata";

                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=biodata&count={2}&packcnt={3}&packidx={4}",
                            _Ns, cmdId[1], 1, queryResultControl.ItensCount, queryResultControl.Index);

                        var f = result[queryResultControl.Index - 1];

                        queryResultData.AppendFormat("biodata pin={0}\tno={1}\tindex={2}\tvalid={3}\tduress={4}\ttype={5}\tmajorver={6}\tminorver={7}\tformat={8}\ttmp={9}" + endDataStr,
                                f.Pin, f.No, f.Index, f.Valid, f.Duress, f.Type, f.Majorver, f.Minorver, f.Format, f.Tmp);
                    }
                    else
                    {
                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=biodata&count={2}&packcnt={3}&packidx={4}",
                        _Ns, cmdId[1], 1, 0, 0);
                    }
                }
                else if (queryTableName.EndsWith("=templatev10"))
                {
                    var result = pinFilter == 0 ? fingerList : fingerList.Where(x => x.Pin == pinFilter).ToList();
                    
                    if (result != null && result.Count > 0)
                    {
                        queryResultControl.ItensCount = result.Count;
                        queryResultControl.Index++;
                        queryResultControl.Table = "templatev10";

                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=templatev10&count={2}&packcnt={3}&packidx={4}",
                         _Ns, cmdId[1], 1, queryResultControl.ItensCount, queryResultControl.Index);

                        var f = result[queryResultControl.Index - 1];

                        queryResultData.AppendFormat("templatev10 size={0}\tuid={1}\tpin={2}\tfingerid={3}\tvalid={4}\ttemplate={5}\tresverd={6}\tendtag={7}" + endDataStr,
                                f.Tmp.Length, f.Index, f.Pin, f.No, f.Valid, f.Tmp, "", "");
                    }
                    else
                    {
                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=templatev10&count={2}&packcnt={3}&packidx={4}",
                        _Ns, cmdId[1], 1, 0, 0);
                    }
                }
                else if (queryTableName.EndsWith("=biophoto"))
                {
                    var result = pinFilter == 0 ? faceList : faceList.Where(x => x.PIN == pinFilter).ToList();
                    if (result != null && result.Count > 0)
                    {
                        queryResultControl.ItensCount = result.Count;
                        queryResultControl.Index++;
                        queryResultControl.Table = "biophoto";

                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=biophoto&count={2}&packcnt={3}&packidx={4}",
                            _Ns, cmdId[1], 1, queryResultControl.ItensCount, queryResultControl.Index);

                        var f = result[queryResultControl.Index - 1];

                        queryResultData.AppendFormat("biophoto pin={0}\ttype={1}\tsize={2}\tcontent={3}" + endDataStr,
                                f.PIN, f.Type, f.Size, f.Content);
                    }
                    else
                    {
                        queryResultUrl = string.Format("iclock/querydata?SN={0}&type=tabledata&cmdid={1}&tablename=biophoto&count={2}&packcnt={3}&packidx={4}",
                        _Ns, cmdId[1], 1, 0, 0);
                    }
                }
                deviceStatus = EStatus.QUERYDATA;
            }
        }
    }
}
