﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZKPushSimulator
{
    public class Person
    {
        public Person()
        {
        }
        public Person(string bodyData)
        {
            try
            {
                var splitBody = bodyData.Remove('\r').Remove('\n');
                var posIni = bodyData.IndexOf("Pin=");
                var dataAux = bodyData.Replace("=", ":" + Consts.quote).Replace("\t", Consts.quote + ", ").Substring(posIni);
                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>("{" + dataAux + Consts.quote + "}");

                Pin = obj.Pin;
                Password = obj.Password;
                Group = obj.Group;
                Name = obj.Name;
                Privilege = obj.Privilege;
                CardNo = obj.CardNo; 
            }
            catch
            {
            }
        }

        public decimal Pin { get; set; }
        public string Password { get; set; }
        public int Group { get; set; }
        public string Name { get; set; }
        public int Privilege { get; set; }
        public decimal CardNo { get; set; }
    }

    public class Face
    {
        public Face()
        {

        }

        public Face(string bodyData)
        {
            try
            {
                var splitBody = bodyData.Remove('\r').Remove('\n');

                var posDelete = bodyData.IndexOf("DATA DELETE biophoto");

                if(posDelete > 0)
                {
                    var posIni = bodyData.IndexOf("Pin=");

                    if(posIni > 0)
                    {
                        var pin = bodyData.Substring(posIni + 4, bodyData.Length - 2 - (posIni + 4));
                        PIN = decimal.Parse(pin);
                    }
                }
                else
                {
                    var posIni = bodyData.IndexOf("PIN=");
                    var posType = bodyData.IndexOf("Type=");
                    var posSize = bodyData.IndexOf("Size=");
                    var posContent = bodyData.IndexOf("Content=");

                    var pin = bodyData.Substring(posIni + 4, bodyData.IndexOf('\t', posIni) - posIni - 4);
                    var type = bodyData.Substring(posType + 5, bodyData.IndexOf('\t', posType) - posType - 5);
                    var ssize = bodyData.Substring(posSize + 5, bodyData.IndexOf('\t', posSize) - posSize - 5);
                    var content = bodyData.Substring(posContent + 8, bodyData.Length - posContent - 8);

                    PIN = decimal.Parse(pin);
                    Type = int.Parse(type);
                    Size = int.Parse(ssize);
                    Content = content;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Face parse Err.: " + ex.Message);
            }
        }

        public decimal PIN { get; set; }
        public int Type { get; set; }
        public int Size { get; set; }
        public string Content { get; set; }
        public int RepeatInSec { get; set; }
        public int CounterRepeat { get; set; }
    }

    public class Finger
    {
        public Finger()
        {

        }

        public Finger(string bodyData)
        {
            try
            {
                var splitBody = bodyData.Remove('\r').Remove('\n');
                var posIni = bodyData.IndexOf("Pin=");
                var dataAux = bodyData.Replace("=", ":" + Consts.quote).Replace("\t", Consts.quote + ", ").Substring(posIni);
                var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Finger>("{" + dataAux + Consts.quote + "}");

                Pin = obj.Pin;
                No = obj.No;
                Index = obj.Index;
                Valid = obj.Valid;
                Duress = obj.Duress;
                Type = obj.Type;
                Majorver = obj.Majorver;
                Minorver = obj.Minorver;
                Format = obj.Format;
                Tmp = obj.Tmp; 
            }
            catch
            {
            }
        }
        public decimal Pin { get; set; }
        public int No { get; set; }
        public int Index { get; set; }
        public int Valid { get; set; }
        public int Duress { get; set; }
        public int Type { get; set; }
        public int Majorver { get; set; }
        public int Minorver { get; set; }
        public int Format { get; set; }
        public string Tmp { get; set; }
        public int RepeatInSec { get; set; }
        public int CounterRepeat { get; set; }
    }

    public class Record
    {
        public decimal Pin { get; set; }
        public decimal CardNo { get; set; }
        public int EventAddr { get; set; }
        public int Event { get; set; }
        public int InoutStatus { get; set; }
        public int VerifyType { get; set; }
        public int Index { get; set; }
        public int SiteCode { get; set; }
        public int LinkId { get; set; }
        public int MaskFlag { get; set; }
        public decimal Temperature { get; set; }
        public int RepeatInSec { get; set; }
        public int CounterRepeat { get; set; }

    }

    public class Action
    {
        public string Sensor { get; set; }
        public string Relay { get; set; }
        public string Alarm { get; set; }
        public string Door { get; set; }
        public int RepeatInSec { get; set; }
        public int CounterRepeat { get; set; }
    }

    public class Palm
    {
        public decimal Pin { get; set; }
        public int No { get; set; }
        public int Valid { get; set; }
        public int Duress { get; set; }
        public int Type { get; set; }
        public int Majorver { get; set; }
        public int Minorver { get; set; }
        public int Format { get; set; }
        public List<string> TmpLst { get; set; }
        public int RepeatInSec { get; set; }
        public int CounterRepeat { get; set; }
    }

    public class Device
    {
        public string ServerUrl { get; set; }
        public string Ns { get; set; }
        public string AuthType { get; set; }
        public bool SaveImageFace { get; set; }
        public List <Person> PersonList { get; set; }
        public List<Face> FaceList { get; set; }
        public List<Finger> FingerList { get; set; }
        public List<Face> NewFaceList { get; set; }
        public List<Finger> NewFingerList { get; set; }
        public List<Record> RecordEventList { get; set; }
        public List<Action> ActionEventList { get; set; }
        public List<Palm> PalmList { get; set; }
        public List<Palm> NewPalmList { get; set; }

    }
}
