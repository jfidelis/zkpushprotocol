﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using log4net;

namespace ZKPushSimulator
{
    class Program
    {
        private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static private Dictionary<string, ZKPushClient> dicZKPushClient = new Dictionary<string, ZKPushClient>();
        static void Main(string[] args)
        {
            try
            {
                logger.InfoFormat("ZKPushSimulator Running as {0}", WindowsIdentity.GetCurrent().Name);
                var devicesData = File.ReadAllText("devices.json");
                var deviceList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Device>>(devicesData);

                foreach (var d in deviceList)
                {
                    dicZKPushClient.Add(d.Ns, new ZKPushClient(d, logger));
                }

                foreach (var client in dicZKPushClient)
                {
                    client.Value.Start();
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine("Set devices from file json - Error: {0}", ex.Message);
            }

            while (true)
            {
                System.Threading.Thread.Sleep(500);
            }
         
        }
    }
}
