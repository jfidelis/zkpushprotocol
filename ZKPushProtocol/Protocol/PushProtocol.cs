﻿using NHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ZKPushProtocol
{
    public class PushProtocol
    {
        public static void ResponseRegistration(TcpClient client, bool regOk)
        {
            PushResponseData data = new PushResponseData();

            if (regOk)
                data = ResponseOK("RegistryCode=");
            else
                data = ResponseOK("OK");

            ProcessSend(client, data);
        }

        public static void ResponseRegistration(HttpRequestEventArgs context, bool regOk)
        {
            string data;

            if (regOk)
                data = "RegistryCode=";
            else
                data = "OK";

            ResponseOk(context, data);
        }

        public static void ResponseOk(TcpClient client)
        {
            PushResponseData data = new PushResponseData();
            data = ResponseOK("OK");
            ProcessSend(client, data);
        }

        public static void ResponseOk(HttpRequestEventArgs context, string data)
        {
            context.Response.StatusCode = 200;
            context.Response.ContentType = "text/plain";
            context.Response.Headers.Add("Server", "ZK Brasil/1.0");
            context.Response.Headers.Add("Content-Type", "text/plain");
            context.Response.Headers.Add("Accept-Ranges", "bytes");
            context.Response.Headers.Add("Date", getDateTime());

            ProcessSend(context, data);
        }

        public static void ResponseOk2(TcpClient client)
        {
            PushResponseData data = new PushResponseData();
            data = ResponseOK2("OK");
            ProcessSend(client, data);
        }

        public static void ResponseOk2(HttpRequestEventArgs context)
        {
            ResponseOk(context, "OK");
        }

        private static string getDateTime()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
            DateTime dt = (DateTime.Now).ToUniversalTime();

            string ast = dt.ToString("r");
            //            ast.Replace("GMT", "America / Sao_Paulo");
            return ast;
        }

        public static void ResponseSetOptions(TcpClient client, string data, bool withHeadescontentType)
        {
            PushResponseData resposeData = new PushResponseData();

            resposeData.StatusCode = "200 OK";
            resposeData.Headers.Add("Server", "ZK Brasil/1.0");
            if(withHeadescontentType)
                resposeData.Headers.Add("Content-Type", "text/plain;charset=UTF-8");
            resposeData.Headers.Add("Content-Length", data.Length.ToString());
            resposeData.Headers.Add("Date", getDateTime());
            resposeData.BodyData = data;

            ProcessSend(client, resposeData);
        }

        public static void ResponseSetOptions(HttpRequestEventArgs context, string data, bool withHeadescontentType)
        {
            context.Response.StatusCode = 200;
            if (withHeadescontentType)
                context.Response.ContentType = "text/plain;charset=UTF-8";
            context.Response.Headers.Add("Date", getDateTime());
            context.Response.Headers.Add("Server", "ZK Brasil/1.0");

            ProcessSend(context, data);
        }

        public static void ResponsePush(TcpClient client, PushData pushData)
        {
            StringBuilder dataSb = new StringBuilder();

            dataSb.AppendLine();
            dataSb.AppendLine($"ServerVersion={ pushData.ServerName }");
            dataSb.AppendLine($"ServerName={ pushData.ServerName }");
            dataSb.AppendLine($"PushVersion={ pushData.PushVersion }");
            dataSb.AppendLine($"ErrorDelay={ pushData.ErrorDelay }");
            dataSb.AppendLine($"RequestDelay={ pushData.RequestDelay }");
            dataSb.AppendLine($"TransTimes={ pushData.TransTimes }");
            dataSb.AppendLine($"TransInterval={ pushData.TransInterval }");
            dataSb.AppendLine($"RealTime={ pushData.RealTime }");
            dataSb.AppendLine($"TransTables={ pushData.RealTime }");
            dataSb.AppendLine($"TimeZone={ pushData.TimeZone }");
            dataSb.AppendLine($"TimeoutSec={ pushData.TimeoutSec }");

            PushResponseData data = new PushResponseData();
            data = ResponseOK(dataSb.ToString());
            ProcessSend(client, data);

        }

        public static void ResponsePush(HttpRequestEventArgs context, PushData pushData)
        {
            StringBuilder dataSb = new StringBuilder();

            dataSb.AppendLine();
            dataSb.AppendLine($"ServerVersion={ pushData.ServerName }");
            dataSb.AppendLine($"ServerName={ pushData.ServerName }");
            dataSb.AppendLine($"PushVersion={ pushData.PushVersion }");
            dataSb.AppendLine($"ErrorDelay={ pushData.ErrorDelay }");
            dataSb.AppendLine($"RequestDelay={ pushData.RequestDelay }");
            dataSb.AppendLine($"TransTimes={ pushData.TransTimes }");
            dataSb.AppendLine($"TransInterval={ pushData.TransInterval }");
            dataSb.AppendLine($"RealTime={ pushData.RealTime }");
            dataSb.AppendLine($"TransTables={ pushData.TransTables }");
            dataSb.AppendLine($"TimeZone={ pushData.TimeZone }");
            dataSb.AppendLine($"TimeoutSec={ pushData.TimeoutSec }");

            context.Response.StatusCode = 200;
            context.Response.ContentType = "text/plain";
            context.Response.Headers.Add("Server", "ZK Brasil/1.0");
            context.Response.Headers.Add("Content-Type", "text/plain");
            context.Response.Headers.Add("Accept-Ranges", "bytes");
            context.Response.Headers.Add("Date", getDateTime());

            ProcessSend(context, dataSb.ToString());

        }

        public static void ResponseContinue(TcpClient client, string data)
        {
            PushResponseData resposeData = new PushResponseData();
            resposeData.StatusCode = "200 OK";
            resposeData.Headers.Add("Server", "ZK Brasil/1.0");
            resposeData.Headers.Add("Accept", "application/push");
            resposeData.Headers.Add("Content-Type", "application/push;charset=UTF-8");
            resposeData.Headers.Add("Content-Length", data.Length.ToString());
            resposeData.BodyData = data;

            ProcessSend(client, resposeData);
        }

        public static void ResponseContinue(HttpRequestEventArgs context, string data)
        {
            StringBuilder sb = new StringBuilder(data);
            context.Response.StatusCode = 200;
            context.Response.ContentType = "application/push";
            //context.Response.Headers.Add("Server", "ZK Brasil/1.0");
            //context.Response.Headers.Add("Accept", "application/push");
            //context.Response.Headers.Add("Date", getDateTime());

            ProcessSend(context, sb.ToString());
        }

        public static string CmdDateTime(DateTime dateTime, int seq )
        {
            DateTime dt = dateTime; //.ToUniversalTime();
            long currentDate = OldEncodeTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
            string data = string.Format("C:{0}:SET OPTIONS DateTime={1}\r\n\r\n\r\n", seq, currentDate);
            return data;
        }

        public static string CmdSendPerson(PersonData person, int seq)
        {

            //DateTime dtSt = person.StartTime.ToUniversalTime();
            //DateTime dtEd = person.EndTime.ToUniversalTime();

            //long dtStL = OldEncodeTime(dtSt.Year, dtSt.Month, dtSt.Day, dtSt.Hour, dtSt.Minute, dtSt.Second);
            //long dtEdL = OldEncodeTime(dtEd.Year, dtEd.Month, dtEd.Day, dtEd.Hour, dtEd.Minute, dtEd.Second);

            //string data = string.Format("C:{0}:DATA UPDATE user CardNo={1} Pin={2} Password={3} Group={4} StartTime={5} EndTime={6} Name={7} Privilege={8}\r\n", 
            //    seq, person.CardNumber, person.Pin, person.PassWord, person.Group, dtStL, dtEdL, person.Name, (int)person.Privilege);

            //"C:{0}:DATA(SP)UPDATE$(SP)user$(SP)CardNo=${XXX}$(HT)Pin=${XXX}$(HT)Password=${XXX}$(HT)Group=${XXX}$(HT)StartTime =${ XXX}$(HT)EndTime =${ XXX}$(HT)Name =${ XXX}$(HT)Privilege =${ XXX}\r\n"



            string data = string.Format("C:{0}:DATA UPDATE user Pin={1}\tPassword={2}\tGroup={3}\tName={4}\tPrivilege={5}\tCardNo={6}\r\n",
                seq, person.Pin, person.PassWord, person.Group, person.Name, (int)person.Privilege, person.CardNumber);

            return data;
        }

        public static string CmdSendBiometricData(BiometricData biometric, int seq)
        {
            string data = string.Empty;
            // biodata pin=1	no=7	index=0	valid=1	duress=0	type=1	majorver=12	minorver=0	format=0	tmp=TZNTUjIzAAAE0NMFBQUHCc7QAAAc0WkAAAA+BGcnl9CsAbJkjAAFANeNdgG+ABlkCQCT0K1legDbANFiXtCUAUFkewA9AKaXugFlACZknAD10CE/SQB0AJNkcdBIAW1k0gDZAV+yjAEtAIpHWwDA0HdlqwDNAKVkydC/AT9kkQBFAI60fAF7AApkvABr0F5lkQAAAUxkhtABAJZBjgDQAfe0gQEaAZJIfgBF0CFl7gANAZY86dBAAShkngBmAEG0uwGPADJkdgDk0FZllQDlALlj1dB+AadkUgB7ADO07AGdADVkdwAK0XNa0wBcAGZk1NATANRkCgFBADXviAEyAYFNcQAm0JdVD7BtFNjfHSw9IDiZEraYFHjxFDC6IAW/0GtCzaIEIVFnJSLiELV7GJ0FItuyJxe9HHqYHaty2Cj/8EVAJezrKkIKKPU2MBviCwHJF+jJHJyOIMQm30yH8n9+JBoGKl70J7h/KUgMLG5PMx85JFYVMCb38TBI7groJfDjJooON51iP3QXCt1CIBXYIrfpRNIE4FMZ+er1MgdqRYcrFPMXE5OjGbgBIc7hEbjqFVAJ5H7g8rGMJbDfJ8WTL0zpLyEKFYBFHYv7JClFLARYyCmvxYGBLNzJM/WdCYRUHDrVJt03MJnZDnbzFT2W0oXjy7wMC430Dy8NHMWiGy0ED/+kFVe6GTldHdRyyuCbyiEQGrH8HwMJJMeLJaD6JIO4MmQoH6d/IJn4+azlmsn9JbeFMh0IOm8DQOySD51WIX8fI5NyIiIT0V+CzDSKIrXtKsoMEQn2H6LbI88kKwDIGkvzHZNo2/0h8GvbGSL7HIV1Jq2AIpsEJUnbKhUmMGIjM4QX59KN9i8FKeyOLm72GsSfIh8XJJ4sJYKvG059Ix/84YCH9b16CuR5HvdiGnWAO1d9JrI5OddaQMKFROmO37HrzcePMrrrMwP4JPF3KF8MQf6rQhIjMgCfYF4BwAAUj2weAwAAPhJhQnqYuqeNtM1bIC7RAMIU7AcADwoYhVILALMIF4FKQt0BpAsXwEYF/WCCBQHZDBxUwQCIwRJYEwCAFNbAURD+/v3/aFn6BgSlHQ1AwQ0AfycS5f7+Vv5bH8VlNS3+///+/z4E/fubwP7A///AO/36E/7+//7/DMVeOSf//v/8/8A7wvotAgG+QRr+wwDqlCZLwQYAdo1wki4ZAU9X4P8+/cQt//7//sDABf3EEP/A/v///joKBAhjJjjAwf6SBASnbGqlDQB/qgb//sH//y7+A8WX8Fz6CAB4dGBYl8TYAXh/V8LDScQI0NWFMP/ARgT9++AEAZWGJCrPAL9CMTrA/lQFxXiXlqATAMyXrbWExxPEwJHAbwnF7qXgNvzBNQQQEjjTuAIBnqQ9+8MAXGQ7w8CRCACbtTAR/sXB/8QHxVO758DBw5oGAAjCOS8zJQBG2b2OwPoRL/z9Hv7/Okf6EP9B///A/Tr+PtUBSvggfATFsuCDHh8AVeq0OkHFLUf9+yT//znB+y7+/v/B/P45wADQXfAgmAYAkvIpT8AIEL0ITzn++i3/PwcQYxfTnZnXEcoZXsH+OykCwM4eWsL7QpdCBNtCAAAAC0WXAECSAAAAAAAWxQAE0AcBAAAAAMUAQZI=
            //"C:{0}:DATA UPDATE biodata Pin={1}\tNo={2}\tIndex={3}\tValid={4}\tDuress={5}\tType={6}\tMajorver={7}\tMinorver={8}\tFormat={9}\tTmp={10}\n";

            data = string.Format("C:{0}:DATA UPDATE biodata Pin={1}\tNo={2}\tIndex={3}\tValid={4}\tDuress={5}\tType={6}\tMajorver={7}\tMinorver={8}\tFormat={9}\tTmp={10}\r\n",
                seq, 
                biometric.Pin,
                biometric.No,
                biometric.Index,  
                biometric.Valid ? 1 : 0,
                biometric.Duress ? 1 : 0,
                (int)biometric.BiopetricDataType, 
                biometric.MajorVerion, 
                biometric.MinorVersion,
                biometric.Format, 
                biometric.Template);

            return data.ToString();
        }

        public static string CmdSendBiometricDataCollect(BiometricDataCollect biometricDatas, int seq)
        {
            string data = string.Empty;

            data = string.Format("C:{0}:DATA UPDATE biodata ", seq);

            foreach (var biometric in biometricDatas)
            {
                data += string.Format("Pin={0}\tNo={1}\tIndex={2}\tValid={3}\tDuress={4}\tType={5}\tMajorVer={6}\tmiNorver={7}\tFormat={8}\tTmp={9}\r\n", 
                biometric.Pin,
                biometric.No,
                biometric.Index,
                biometric.Valid ? 1 : 0,
                biometric.Duress ? 1 : 0,
                (int)biometric.BiopetricDataType,
                biometric.MajorVerion,
                biometric.MinorVersion,
                biometric.Format,
                biometric.Template);
            }
            return data;
        }

        public static string CmdGetAllFaces(int seq)
        {
            string data = string.Format("C:{0}:DATA QUERY tablename=biophoto,fielddesc=*,filter=*\r\n", seq);
            return data;
        }

        public static string CmdGetAllTemplates(int seq)
        {
            string data = string.Format("C:{0}:DATA QUERY tablename=biodata,fielddesc=*,filter=*\r\n", seq);
            return data;
        }

        public static string CmdSendFaceData(FaceData faceData, int seq)
        {
            //string str = string.Format("C:{0}:DATA UPDATE biophoto PIN={1}\tType={2}\tSize={3}\tContent={4}\r\n", idx, us.pin, us.type, us.size, Encoding.ASCII.GetString(us.data));
            string data = string.Empty;

            data = string.Format("C:{0}:DATA UPDATE biophoto PIN={1}\tType={2}\tSize={3}\tContent={4}\r\n",
                seq,
                faceData.Pin, 
                faceData.Type,
                faceData.Size,
                faceData.Content 
                );

            return data;
        }

        public static string CmdExcludePerson(int idPerson, int seq)
        {
            //C:$(CmdID):DATA$(SP)DELETE$(SP)$(TableName)$(SP)$(Cond)
            string data = string.Format("C:{0}:DATA DELETE user Pin={1}\r\n", seq, idPerson);
            return data;
        }

        public static string CmdGetPerson(int idPerson, int seq)
        {
            // string str = string.Format("C:{0}:DATA QUERY tablename=user,fielddesc=*,filter=Pin=1"
            string data = string.Format("C:{0}:DATA QUERY tablename=user,fielddesc=*,filter=Pin={1}\r\n", seq, idPerson);
            return data;
        }

        public static string CmdGetTemplates(int idPerson, int seq)
        {
            string data = string.Format("C:{0}:DATA QUERY tablename=biodata,fielddesc=*,filter=Pin={1}\tType=1\r\n", seq, idPerson);
            return data;
        }

        public static string CmdGetFaceByIdPerson(int idPerson, int seq)
        {
            string data = string.Format("C:{0}:DATA QUERY tablename=biophoto,fielddesc=*,filter=Pin={1}\r\n", seq, idPerson);
            return data;
        }

        public static string CmdRtData(DateTime dateTime, int ServerTZ)
        {
            DateTime dt = dateTime.AddHours(-8);
            long currentDate = OldEncodeTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
            string data = string.Format("DateTime={0},MachineTZ=-0300,ServerTZ={1:D4}", currentDate, ServerTZ);
            return data;
        }

        public static string CmdGetDateTime(int seq)
        {
            string data = string.Empty;
            data = string.Format(@"C:{0}:GET OPTIONS DateTime,\r\n", seq);
            return data;
        }

        public static string CmdControlDevice(int seq, ControlDeviceData deviceData)
        {
            string data = string.Empty;
            data = string.Format(@"C:{0}:CONTROL DEVICE {1}\r\n", seq, deviceData.GetCommand());
            return data;
        }

        public static string CmdGetConfigs(int seq, ConfigTypeEnum configType)
        {
            //  C:$(CmdID):GET$(SP)OPTIONS$(SP) list of parameter name 
            //~IsOnlyRFMachine,~MaxUserCount,~MaxAttLogCount,~MaxUserFingerCount,IPAddress,NetMask,GATEIPAddress,~ZKFPVersion,IclockSvrFun,OverallAntiFunOn,~REXInputFunOn,~CardFormatFunOn,~SupAuthrizeFunOn,~ReaderCFGFunOn,~ReaderLinkageFunOn,~RelayStateFunOn,~Ext485ReaderFunOn,~TimeAPBFunOn,~CtlAllRelayFunOn,~LossCardFunOn,SimpleEventType,VerifyStyles,EventTypes,DisableUserFunOn,DeleteAndFunOn,LogIDFunOn,DateFmtFunOn,DelAllLossCardFunOn,AutoClearDay,FirstDelayDay,DelayDay,StopAllVerify,FvFunOn,FaceFunOn,FingerFunOn,CameraOpen,IsSupportNFC,~MaxFingerCount,~DSTF,Reader1IOState,MachineTZFunOn,BioPhotoFun,BioDataFun,VisilightFun,~MaxBioPhotoCount
            string data = string.Empty;

            switch (configType)
            {
                case ConfigTypeEnum.DeviceVersion:
                    data = string.Format(@"C:{0}:GET OPTIONS ~DeviceName,FirmVer,PushVersion", seq);
                    break;
                case ConfigTypeEnum.Network:
                    data = string.Format(@"C:{0}:GET OPTIONS IPAddress,NetMask,GATEIPAddress", seq);
                    break;
                case ConfigTypeEnum.Capacity:
                    data = string.Format(@"C:{0}:GET OPTIONS ~MaxUserCount,~MaxAttLogCount,~MaxUserFingerCount,~MaxFingerCount,~MaxBioPhotoCount", seq);
                    break;
                case ConfigTypeEnum.AccessControl:
                    data = string.Format(@"C:{0}:GET OPTIONS SimpleEventType,VerifyStyles,EventTypes", seq);
                    break;
                case ConfigTypeEnum.All:
                default:
                    data = string.Format(@"C:{0}:GET OPTIONS ~DeviceName,FirmVer,PushVersion,IPAddress,NetMask,GATEIPAddress,~MaxUserCount,~MaxAttLogCount,~MaxUserFingerCount,~MaxFingerCount,~MaxBioPhotoCount,SimpleEventType,VerifyStyles,AutoServerFunOn,AutoServerMode,MultiCardInterTimeFunOn", seq);
                    break;
            }
            return data;
        }

        public static string CmdSendConfigs(int seq, ConfigData configData)
        {
            string data = string.Empty;

            data = string.Format(@"C:{0}:SET OPTIONS SimpleEventType={1},VerifyStyles={2},EventTypes={3}", seq, 
                configData.SimpleEventType, 
                configData.VerifyStylesData.GetDataHex(),
                configData.EventTypes);

            return data;
        }

        public static string CmdAutoServerFun(int seq, bool on)
        {
            string data = string.Format("C:{0}:SET OPTIONS AutoServerFunOn={1}\r\n", seq, on ? 1 : 0);
            return data;
        }

        public static string CmdSetMultiCardInterTimeFunOn(int seq, bool on)
        {
            string data = string.Format("C:{0}:SET OPTIONS MultiCardInterTimeFunOn={1}\r\n", seq, on ? 1 : 0);
            return data;
        }

        public static string CmdAutoServerMode(int seq, int mode)
        {
            string data = string.Format("C:{0}:SET OPTIONS AutoServerMode={1}\r\n", seq, mode);
            return data;
        }

        public static string CmdUserAuthorize(int seq, int idPerson)
        {
            string data = string.Format("C:{0}:DATA UPDATE userauthorize Pin={1}\tAuthorizeTimezoneId=1\tAuthorizeDoorId=1\tDevId=1\r\n", seq, idPerson);
            return data;
        }

        private static PushResponseData ResponseOK(string data)
        {
            PushResponseData resposeData = new PushResponseData();
            resposeData.StatusCode = "200 OK";
            resposeData.Headers.Add("Server", "ZK Brasil/1.0");
            resposeData.Headers.Add("Content-Type", "text/plain");
            resposeData.Headers.Add("Accept-Ranges", "bytes");
            resposeData.Headers.Add("Content-Length", data.Length.ToString());
            resposeData.Headers.Add("Date", getDateTime());
            //resposeData.Headers.Add("Connection", "close");
            resposeData.BodyData = data;

            return resposeData;
        }

        private static PushResponseData ResponseOK2(string data)
        {
            PushResponseData resposeData = new PushResponseData();
            resposeData.StatusCode = "200 OK";
            resposeData.Headers.Add("Server", "ZK Brasil/1.0");
            resposeData.Headers.Add("Content-Length", data.Length.ToString());
            resposeData.Headers.Add("Date", getDateTime());
            resposeData.BodyData = data;

            return resposeData;
        }

        private static void ProcessSend(TcpClient client, PushResponseData message)
        {
            byte[] messageByte = Encoding.ASCII.GetBytes(message.ToString());
            client.Client.Send(messageByte);
        }


        private static void ProcessSend(HttpRequestEventArgs context, string message)
        {
            using (var writer = new StreamWriter(context.Response.OutputStream, new UTF8Encoding(false), message.Length))
            {
                writer.Write(message.ToString());
            }
        }

        private static void ProccessSendBody(TcpClient client, string body)
        {
            byte[] messageByte = Encoding.ASCII.GetBytes(body);
            client.Client.Send(messageByte);
        }

        private static void ProccessSendBody(HttpRequestEventArgs context, string body)
        {
            using (var writer = new StreamWriter(context.Response.OutputStream))
            {
                writer.Write(body);
            }
        }

        public static long OldEncodeTime(int year, int mon, int day, int hour, int min, int sec)
        {
            long res = 0;
            res = ((year - 2000) * 12 * 31 + ((mon - 1) * 31) + day - 1) * (24 * 60 * 60) + (hour * 60 + min) * 60 + sec;
            return res;
        }

        public static DateTime OldDecodeTime(int tt)
        {
            int sec = tt % 60;
            tt /= 60;

            int min = tt % 60;
            tt /= 60;

            int hour = tt % 24;
            tt /= 24;

            int day = tt % 31 + 1;
            tt /= 31;

            int mon = tt % 12 + 1;
            tt /= 12;

            int year = tt + 2000;

            return new DateTime(year, mon, day, hour, min, sec);
        }
    }
}
