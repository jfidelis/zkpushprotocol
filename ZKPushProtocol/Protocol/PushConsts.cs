﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public static class Consts
    {
        public const string FLD_USER_UID = "user uid=";
        public const string FLD_CARDNO = "cardno=";
        public const string FLD_PIN = "pin=";
        public const string FLD_PASSWORD = "password=";
        public const string FLD_GROUP = "group=";
        public const string FLD_STARTTIME = "starttime=";
        public const string FLD_ENDTIME = "endtime=";
        public const string FLD_NAME = "name=";
        public const string FLD_PRIVILEGE = "privilege=";
        public const string FLD_DISABLE = "disable=";
        public const string FLD_VERIFY = "verify=";
        public const string FLD_BIODATA = "biodata ";
        public const string FLD_NO = "no=";
        public const string FLD_INDEX = "index=";
        public const string FLD_VALID = "valid=";
        public const string FLD_DURESS = "duress=";
        public const string FLD_TYPE = "type=";
        public const string FLD_MAJORVER = "majorver=";
        public const string FLD_MINORVER = "minorver=";
        public const string FLD_FORMAT = "format=";
        public const string FLD_TMP = "tmp=";
        public const string FLD_DEVICENAME = "~DeviceName=";
        public const string FLD_FIRMVER = "FirmVer=";
        public const string FLD_IPADDRESS = "IPAddress=";
        public const string FLD_NETMASK = "NetMask=";
        public const string FLD_GATEADDRESS = "GATEIPAddress=";
        public const string FLD_MAXUSERCOUNT = "~MaxUserCount=";
        public const string FLD_MAXATTLOGCOUNT = "~MaxAttLogCount=";
        public const string FLD_MAXUSERFINGERCOUNT = "~MaxUserFingerCount=";
        public const string FLD_MAXFINGERCOUNT = "~MaxFingerCount=";
        public const string FLD_MAXBIOPHOTOCOUNT = "~MaxBioPhotoCount=";
        public const string FLD_SIMPLEEVENTTYPE = "SimpleEventType=";
        public const string FLD_VERIFYSTYLES = "VerifyStyles=";
        public const string FLD_EVENTTYPES = "EventTypes=";
        public const string FLD_TIMERT = "time=";
        public const string FLD_EVENTADDR = "eventaddr=";
        public const string FLD_EVENT = "event=";
        public const string FLD_INOUTSTATUS = "inoutstatus=";
        public const string FLD_VERIFYTYPE = "verifytype=";
        public const string FLD_SITECODE = "sitecode=";
        public const string FLD_LINKID = "linkid=";
        public const string FLD_SENSOR = "sensor=";
        public const string FLD_RELAY = "relay=";
        public const string FLD_ALARM = "alarm=";
        public const string FLD_DOOR = "door=";
        public const string FLD_BIOPHOTO = "biophoto";
        public const string FLD_FILENAME = "filename=";
        public const string FLD_SIZE = "size=";
        public const string FLD_CONTENT = "content=";
        public const string FLD_AUTOSERVERFUNON = "AutoServerFunOn=";
        public const string FLD_AUTOSERVERMODE = "AutoServerMode=";
        public const string FLD_MULTICARDINTERTIMEFUNON = "MultiCardInterTimeFunOn=";
    }
}
