﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public enum PrivilegeTypeEnum
    {
        Normal = 0,
        Register = 2,
        Admin = 6,
        User = 10,
        SuperAdmin = 14
    }

    public enum PushApiType
    {
        none,
        cdata,
        getrequest,
        rtdata,
        registry,
        push,
        devicecmd,
        querydata,
        ping
    }

    public enum BiopetricDataTypeEnum
    {
        NotSupported = -1,
        Universal = 0,
        Fingerprint = 1,
        Face = 2,
        Voiceprint = 3,
        Iris = 4,
        Retina = 5,
        PalmPrint = 6,
        FingerVein = 7,
        Palm = 8,
        LightVisibleFace = 9
    }

    public enum CommandTypeEnum
    {
        None,
        DateTime,
        PersonRemove,
        PersonAdd,
        GetPerson,
        GetBiometrics,
        GetConfig,
        ConfigData,
        BiometricAdd,
        FaceAdd,
        SendConfigs,
        SetUserAuthorize,
        GetFaceByIdPerson,
        GetDateTime,
        ControlDevice,
        SetAutoServerFun,
        SetAutoServerMode,
        GetAllTemplates,
        GetAllFaces,
        SetMultiCardInterTimeFunOn
    }

    public enum RealTimeEvTypeEnum
    {
        LogAccess,
        LogStatus
    }

    public enum ConfigTypeEnum
    {
        DeviceVersion,
        Network,
        Capacity,
        AccessControl,
        All
    }

    public enum VerifyTypeEnum
    {
        FingerVein_Face_Fingerprint_Or_Password = 0,
        Only_Fingerprint = 1,
        Employee_Number_Verification = 2,
        Only_Password = 3,
        Only_Card = 4,
        Fingerprint_Or_Password = 5,
        Fingerprint_Or_Card = 6,
        Card_Or_Password = 7,
        EmployeeNumber_And_Fingerprint = 8,
        Fingerprint_And_Password = 9,
        Card_And_Fingerprint = 10,
        Card_And_Password = 11,
        Fingerprint_And_Password_And_Card = 12,
        EmployeeNumber_And_Fingerprint_And_Password = 13,
        EmployeeNumber_And_Fingerprint_Or_Card_And_Fingerprint = 14,
        Face = 15,
        Face_And_Fingerprint = 16,
        Face_And_Password = 17,
        Face_And_Card = 18,
        Face_And_Fingerprint_And_Card = 19,
        Face_And_Fingerprint_And_Password = 20,
        Finger_Vein = 21,
        Finger_Vein_And_Password = 22,
        Finger_Vein_And_Card = 23,
        Finger_Vein_And_Password_And_Card = 24
    }
}
