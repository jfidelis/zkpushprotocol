﻿using NHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace ZKPushProtocol.Http
{
    public class HttpServerParser 
    {
        private HttpServer httpServer { get; set; }

        private List<DeviceContext> deviceContexts;

        public delegate void DelOnConnectedEvent(int id);
        public event DelOnConnectedEvent onConnectedEvent;

        public delegate void DelOnCommandStatus(int id, CommandTypeEnum commandType, int status, object data = null);
        public DelOnCommandStatus onCommandStatusEvent;

        public delegate void DelOnRealTimeEvent(int id, RealTimeEvTypeEnum typeEnum, object data);
        public DelOnRealTimeEvent onRealTimeEvent;

        public delegate void DelOnNewBiometricsEvent(int id, BiometricDataCollect data);
        public DelOnNewBiometricsEvent onNewBiometricsEvent;

        public delegate void DelOnNewFaceEvent(int id, FaceData data);
        public DelOnNewFaceEvent onNewFaceEvent;

        private string _host = "127.0.0.1";
        private int _port = 8088;

        public HttpServerParser(List<DeviceContext> deviceContexts, string host, int port)
        {
            this.deviceContexts = deviceContexts;
            this._host = host;
            this._port = port;
            httpServer = new HttpServer();
            httpServer.EndPoint = new IPEndPoint(IPAddress.Parse(host), port);
            httpServer.RequestReceived += HttpServer_RequestReceived;
            httpServer.StateChanged += HttpServer_StateChanged;
            httpServer.ReadTimeout = new TimeSpan(0,0,30);
            httpServer.WriteTimeout = new TimeSpan(0, 0, 30);
            httpServer.ReadBufferSize = 1024 * 1000;
            httpServer.WriteBufferSize = 1024 * 1000;
        }


        private DeviceContext GetDeviceAndInclementSeq(int id)
        {
            var device = deviceContexts.Where(x => x.Id == id).SingleOrDefault();
            if (device != null)
            {
                device.SeqCommand++;
                return device;
            }
            else
            {
                throw new Exception("Device id not found in context");
            }
        }

        public void SetDateTime(int id, DateTime dateTime)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.DataSendCache = dateTime;
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.DateTime, PushProtocol.CmdDateTime(dateTime, d.SeqCommand)));
        }

        public void SendPerson(int id, PersonData person)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.PersonAdd, PushProtocol.CmdSendPerson(person, d.SeqCommand)));
        }

        public void SendBiometricData(int id, BiometricData biometric)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.BiometricAdd, PushProtocol.CmdSendBiometricData(biometric, d.SeqCommand)));
        }

        public void SendBiometricDataCollect(int id, BiometricDataCollect biometricDatas)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.BiometricAdd, PushProtocol.CmdSendBiometricDataCollect(biometricDatas, d.SeqCommand)));
        }

        public void SendFaceData(int id, FaceData faceData)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.FaceAdd, PushProtocol.CmdSendFaceData(faceData, d.SeqCommand)));
        }

        public void ExcludePerson(int id, int idPerson)

        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.PersonRemove, PushProtocol.CmdExcludePerson(idPerson, d.SeqCommand)));
        }

        public void GetPerson(int id, int idPerson)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetPerson, PushProtocol.CmdGetPerson(idPerson, d.SeqCommand)));
        }

        public void GetTemplates(int id, int idPerson)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetBiometrics, PushProtocol.CmdGetTemplates(idPerson, d.SeqCommand)));
        }

        public void GetFaceByIdPerson(int id, int idPerson)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetFaceByIdPerson, PushProtocol.CmdGetFaceByIdPerson(idPerson, d.SeqCommand)));
        }

        public void GetDateTime(int id)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetDateTime, PushProtocol.CmdGetDateTime(d.SeqCommand)));
        }

        public void GetConfigs(int id)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetConfig, PushProtocol.CmdGetConfigs(d.SeqCommand, ConfigTypeEnum.All)));
        }

        public void SendConfigs(int id, ConfigData configData)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.SendConfigs, PushProtocol.CmdSendConfigs(d.SeqCommand, configData)));
        }

        public void SetUserAuthorize(int id, int idPerson)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.SetUserAuthorize, PushProtocol.CmdUserAuthorize(d.SeqCommand, idPerson)));
        }

        public void SendControlDevice(int id, ControlDeviceData deviceData)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.ControlDevice, PushProtocol.CmdControlDevice(d.SeqCommand, deviceData)));
        }

        public void SetAutoServerFun(int id, bool on)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.SetAutoServerFun, PushProtocol.CmdAutoServerFun(d.SeqCommand, on)));
        }

        public void SetAutoServerMode(int id, int mode)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.SetAutoServerMode, PushProtocol.CmdAutoServerMode(d.SeqCommand, mode)));
        }

        public void SetMultiCardInterTimeFunOn(int id, bool on)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.SetMultiCardInterTimeFunOn, PushProtocol.CmdSetMultiCardInterTimeFunOn(d.SeqCommand, on)));
        }

        public void GetAllPersonsData(int id)
        {
            var d = GetDeviceAndInclementSeq(id);
            d.Commands.Add(new Command(d.SeqCommand, CommandTypeEnum.GetAllTemplates, PushProtocol.CmdGetAllTemplates(d.SeqCommand)));

            var d2 = GetDeviceAndInclementSeq(id);
            d2.Commands.Add(new Command(d2.SeqCommand, CommandTypeEnum.GetAllFaces, PushProtocol.CmdGetAllFaces(d2.SeqCommand)));
        }

        private void HttpServer_StateChanged(object sender, EventArgs e)
        {
            var context = (HttpServer)sender;
        }

        public void Start()
        {
            httpServer.Start();
            //var request = (HttpWebRequest)WebRequest.Create(
            //    String.Format("http://{0}/", httpServer.EndPoint)
            //);
        }

        public void Stop()
        {
            httpServer.Stop();
        }

        public HttpServerState State()
        {
            return httpServer.State;
        }

        private void HttpServer_RequestReceived(object sender, HttpRequestEventArgs e)
        {
            string dataBody = string.Empty;
            if (e.Request.ContentLength > 0)
            {
                using (var reader = new StreamReader(e.Request.InputStream))
                {
                    dataBody = reader.ReadToEnd();
                }
            }
            ProcessRequest(e, dataBody);
        }
        private void ProcessRequest(HttpRequestEventArgs context, string dataBody)
        {
            var qs = context.Request.Params;

            if (qs["SN"] != null)
            {
                var sn = qs["SN"];

                var deviceContext = deviceContexts.Where(x => x.SerialNumber == sn).SingleOrDefault();

                if (deviceContext != null)
                {

                    OnConnected(deviceContext.Id);

                    if (context.Request.HttpMethod == "GET")
                    {
                        ProcessMethodGets(context, deviceContext, dataBody);
                    }
                    else if (context.Request.HttpMethod == "POST")
                    {
                        ProcessMethodPosts(context, deviceContext, dataBody);
                    }
                }
            }
        }

        private PushApiType GetApiType(HttpRequest requestData)
        {
            PushApiType pushApi = PushApiType.none;

            var _Uri = requestData.RawUrl;

            if (_Uri.StartsWith("/iclock/cdata"))
                pushApi = PushApiType.cdata;

            else if (_Uri.StartsWith("/iclock/getrequest"))
                pushApi = PushApiType.getrequest;

            else if (_Uri.StartsWith("/iclock/rtdata"))
                pushApi = PushApiType.rtdata;

            else if (_Uri.StartsWith("/iclock/registry"))
                pushApi = PushApiType.registry;

            else if (_Uri.StartsWith("/iclock/push"))
                pushApi = PushApiType.push;

            else if (_Uri.StartsWith("/iclock/devicecmd"))
                pushApi = PushApiType.devicecmd;

            else if (_Uri.StartsWith("/iclock/querydata"))
                pushApi = PushApiType.querydata;

            else if (_Uri.StartsWith("/iclock/ping"))
                pushApi = PushApiType.ping;

            return pushApi;
        }


        private Dictionary<string, string> GetParams(string data)
        {
            Dictionary<string, string> res = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(data) == false)
            {
                var items = data.Split(new char[] { '&' });

                if (items != null)
                {

                    foreach (var item in items)
                    {
                        var posID = item.IndexOf("ID=");
                        if (posID > -1)
                        {
                            string idStr = item.Substring(posID);
                            var id = idStr.Split(new char[] { '=' });
                            if (res.ContainsKey(id[0]) == false)
                                res.Add(id[0], id[1]);
                        }
                        else
                        {
                            var d = item.Split(new char[] { '=' });
                            if (d != null)
                            {
                                if (res.ContainsKey(d[0]) == false)
                                    res.Add(d[0], d[1]);
                            }
                        }
                    }
                }
            }
            return res;
        }

        private Dictionary<string, string> GetQueryStringParams(HttpRequest requestData)
        {
            var QueryString = requestData.Url.ToString();
            return GetParams(QueryString);
        }

        private Dictionary<string, string> GetParamsInBody(string dataBody)
        {
            return GetParams(dataBody);
        }

        private void ProcessMethodGets(HttpRequestEventArgs context, DeviceContext dc, string dataBody)
        {
            switch (GetApiType(context.Request))
            {
                case PushApiType.none:

                    break;
                case PushApiType.cdata:

                    PushProtocol.ResponseOk(context, "OK");

                    break;
                case PushApiType.getrequest:

                    if (dc.Commands?.Count > 0)
                    {
                        if (dc.Commands[0].Data.Length > 1024)
                        {
                            PushProtocol.ResponseContinue(context, dc.Commands[0].Data);
                        }
                        else
                        {
                            PushProtocol.ResponseSetOptions(context, dc.Commands[0].Data, true);
                        }
                        //PushProtocol.ResponseSetOptions(context, dc.Commands[0].Data, true);
                    }
                    else
                    {
                        PushProtocol.ResponseOk(context, "OK");
                    }

                    break;
                case PushApiType.rtdata:
                    //if(dc.Commands?.Count > 0)
                    //{
                    var pos = dc.Commands[0].Data.IndexOf("DateTime=");
                    string rtdata = string.Empty;

                    rtdata = PushProtocol.CmdRtData(dc.DataSendCache != null ? (DateTime)dc.DataSendCache :  DateTime.Now, 0);

                    PushProtocol.ResponseSetOptions(context, rtdata, false);

                    CommandTypeEnum cmdType = CommandTypeEnum.None;

                    // Command
                    if (dc.Commands[0].Data.Contains("DateTime="))
                    {
                        cmdType = CommandTypeEnum.DateTime;
                    }

                    // Create event
                    OnCommandStatusEvent(dc.Id, cmdType, dc.CommandStatus);

                    // Remove
                    dc.Commands.Remove(dc.Commands[0]);

                    //}

                    break;
                case PushApiType.registry:

                    PushProtocol.ResponseRegistration(context, true);

                    break;
                case PushApiType.push:

                    PushProtocol.ResponsePush(context, dc.Configs);

                    break;
                case PushApiType.ping:
                    // This is used for heartbeat maintain of the server. 
                    // When processing the large amount of data upload, use ping to maintain heartbeat, 
                    //when finish the processing, maintain the heartbeat with getrequest 
                    dc.KeepAliveTimeout.Restart();
                    PushProtocol.ResponseOk(context, "OK");

                    break;
                case PushApiType.devicecmd:

                    // Verificar o resultado do comando 

                    // Gerar evento 

                    // Remover da lista

                    // Return OK

                    PushProtocol.ResponseOk(context, "OK");

                    break;
                default:
                    break;
            }
        }

        private void ProcessMethodPosts(HttpRequestEventArgs context, DeviceContext dc, string dataBody)
        {
            switch (GetApiType(context.Request))
            {
                case PushApiType.none:
                    break;
                case PushApiType.cdata:

                    // POST /iclock/cdata?SN=CGFE193460002&table=tabledata&tablename=biodata&count=1 HTTP/1.1  (application/push)

                    // POST /iclock/cdata?SN=CGFE193460002&table=rtlog HTTP/1.1


                    //var paramsRt = requestData.GetQueryStringParams();
                    var paramsRt = context.Request.Params;

                    if (paramsRt["table"] != null)
                    {
                        if (paramsRt["table"] == "rtlog")
                        {
                            //time=2019-12-10 19:20:16	pin=4	cardno=0	eventaddr=1	event=0	inoutstatus=1	verifytype=0	index=271	sitecode=0	linkid=0
                            RealTimeEventData timeEventData = new RealTimeEventData(context.Request, dataBody);
                            OnRealTimeEvent(dc.Id, RealTimeEvTypeEnum.LogAccess, timeEventData);
                        }
                        else if (paramsRt["table"] == "rtstate")
                        {
                            //time=2019-12-10 20:24:59	sensor=00	relay=01	alarm=0200000000000000	door=01
                            RealTimeStatusData statusData = new RealTimeStatusData(context.Request, dataBody);
                            OnRealTimeEvent(dc.Id, RealTimeEvTypeEnum.LogStatus, statusData);
                        }
                        else if (paramsRt["table"] == "tabledata")
                        {
                            if (paramsRt["tablename"] != null)
                            {
                                var tableName = paramsRt["tablename"];
                                if (tableName == "biodata")
                                {
                                    // biodata pin=3	no=3	index=0	valid=1	duress=0	type=1	majorver=12	minorver=0	format=0	tmp=...
                                    BiometricDataCollect biometricDatas = new BiometricDataCollect(context.Request, dataBody);
                                    OnNewBiometricsEvent(dc.Id, biometricDatas);
                                }
                                else if (tableName == "biophoto")
                                {
                                    FaceData faceData = new FaceData(context.Request, dataBody);
                                    OnNewFaceEvent(dc.Id, faceData);
                                }
                            }
                        }
                    }
                    else if (paramsRt["AuthType"] != null)
                    {
                        RealTimeEventData timeEventData = new RealTimeEventData(context.Request, dataBody);
                        OnRealTimeEvent(dc.Id, RealTimeEvTypeEnum.LogAccess, timeEventData);
                    }

                    PushProtocol.ResponseOk(context, "OK");

                    break;
                case PushApiType.getrequest:
                    break;
                case PushApiType.rtdata:
                    break;
                case PushApiType.registry:

                    PushProtocol.ResponseRegistration(context, true);

                    break;
                case PushApiType.push:

                    PushProtocol.ResponsePush(context, dc.Configs);

                    break;
                case PushApiType.devicecmd:

                    // Verificar o resultado do comando 

                    // Gerar evento 

                    // Remover da lista

                    // Return OK
                    var paramS = GetParamsInBody(dataBody);

                    if (paramS.ContainsKey("CMD"))
                    {
                        var cmd = paramS["CMD"];
                        var idCmd = paramS["ID"];
                        var cmdResult = paramS["Return"];
                        dc.CommandStatus = int.Parse(cmdResult);

                        if (cmd.StartsWith("DATA UPDATE"))
                        {
                            // Remove
                            if (dc.Commands?.Count > 0)
                            {
                                OnCommandStatusEvent(dc.Id, dc.Commands[0].CommandType, dc.CommandStatus);
                                dc.Commands.Remove(dc.Commands[0]);
                            }

                        }
                        else if (cmd.StartsWith("DATA DELETE"))
                        {
                            // Create event
                            OnCommandStatusEvent(dc.Id, CommandTypeEnum.PersonRemove, dc.CommandStatus);

                            // Remove
                            if (dc.Commands?.Count > 0)
                                dc.Commands.Remove(dc.Commands[0]);
                        }
                        else if (cmd.StartsWith("GET OPTIONS") || cmd.StartsWith("SET OPTIONS"))
                        {

                            if (dc.Commands?.Count > 0)
                            {
                                OnCommandStatusEvent(dc.Id, dc.Commands[0].CommandType, dc.CommandStatus);
                                if (dc.Commands[0].CommandType != CommandTypeEnum.DateTime)
                                    dc.Commands.Remove(dc.Commands[0]);
                            }
                            else
                            {
                                OnCommandStatusEvent(dc.Id, CommandTypeEnum.GetConfig, dc.CommandStatus);
                            }
                        }
                        else if (cmd.StartsWith("CONTROL DEVICE"))
                        {
                            OnCommandStatusEvent(dc.Id, CommandTypeEnum.ControlDevice, dc.CommandStatus);
                            if (dc.Commands?.Count > 0)
                                dc.Commands.Remove(dc.Commands[0]);
                        }
                    }

                    //var paramQ = GetBodyToString(context);

                    PushProtocol.ResponseOk(context, "OK");

                    break;

                case PushApiType.querydata:

                    var paramQueryString = GetQueryStringParams(context.Request);
                    bool removeCmd = false;

                    if (paramQueryString.ContainsKey("type"))
                    {
                        var queryDataType = paramQueryString["type"];

                        if (queryDataType == "tabledata")
                        {
                            int cmdId = int.Parse(paramQueryString["cmdid"]);
                            var tablename = paramQueryString["tablename"];
                            int count = int.Parse(paramQueryString["count"]);
                            int packcnt = int.Parse(paramQueryString["packcnt"]);
                            int packidx = int.Parse(paramQueryString["packidx"]);

                            if (tablename == "user")
                            {
                                PersonData person = new PersonData(context.Request, dataBody);
                                OnCommandStatusEvent(dc.Id, CommandTypeEnum.GetPerson, dc.CommandStatus, person);
                            }
                            else if (tablename == "biodata")
                            {
                                BiometricDataCollect biometricDataCollect = new BiometricDataCollect(context.Request, dataBody);
                                OnCommandStatusEvent(dc.Id, CommandTypeEnum.GetBiometrics, dc.CommandStatus, biometricDataCollect);
                            }
                            else if (tablename == "biophoto")
                            {
                                FaceData faceData = new FaceData(context.Request, dataBody);
                                OnCommandStatusEvent(dc.Id, CommandTypeEnum.GetFaceByIdPerson, dc.CommandStatus, faceData);
                            }

                            removeCmd = true;

                        }
                        if (queryDataType == "options")
                        {
                            ConfigData configData = new ConfigData(context.Request, dataBody);

                            configData.VerifyStylesData = new VerifyStylesData(configData.VerifyStyles);

                            //var prova = configData.VerifyStylesData.GetDataHex();

                            OnCommandStatusEvent(dc.Id, CommandTypeEnum.ConfigData, dc.CommandStatus, configData);
                            removeCmd = true;
                        }
                    }

                    if (dc.Commands?.Count > 0 && removeCmd)
                        dc.Commands.Remove(dc.Commands[0]);

                    PushProtocol.ResponseOk(context, "OK");

                    break;

                default:
                    break;
            }
        }

        private void OnConnected(int id)
        {
            if (onConnectedEvent != null)
            {
                this.onConnectedEvent(id);
            }
        }

        private void OnCommandStatusEvent(int id, CommandTypeEnum commandType, int status, object data = null)
        {
            if (onCommandStatusEvent != null)
            {
                this.onCommandStatusEvent(id, commandType, status, data);
            }
        }

        private void OnRealTimeEvent(int id, RealTimeEvTypeEnum typeEnum, object data)
        {
            if (onRealTimeEvent != null)
            {
                this.onRealTimeEvent(id, typeEnum, data);
            }
        }

        private void OnNewBiometricsEvent(int id, BiometricDataCollect data)
        {
            if (onNewBiometricsEvent != null)
            {
                this.onNewBiometricsEvent(id, data);
            }
        }

        private void OnNewFaceEvent(int id, FaceData data)
        {
            if (onNewFaceEvent != null)
            {
                this.onNewFaceEvent(id, data);
            }
        }
    }
}
