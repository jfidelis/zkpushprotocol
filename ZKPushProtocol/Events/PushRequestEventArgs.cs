﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ZKPushProtocol
{
    /// <summary>
    /// Provides data for the Push Request Event event
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class PushRequestEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        public TcpClient Client { get; private set; }
        /// <summary>
        /// Gets the push request data.
        /// </summary>
        /// <value>
        /// The push request data.
        /// </value>
        public PushRequestData PushRequestData { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PushRequestEventArgs"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="pushRequestData">The push request data.</param>
        public PushRequestEventArgs(TcpClient client, PushRequestData pushRequestData)
        : base()
        {
            this.Client = client;
            this.PushRequestData = pushRequestData;
        }
    }
}
