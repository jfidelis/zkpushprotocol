﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class DeviceContext
    {
        public int Id { get; set; }
        public PushData Configs { get; set; }
        public string SerialNumber { get; set; }

        public bool Conected { get; set; }

        public Stopwatch KeepAliveTimeout { get; set; }

        public List<Command> Commands = new List<Command>();

        public int CommandStatus { get; set; }

        public int SeqCommand { get; set; }

        public object DataSendCache { get; set; }
    }

    public class Command
    {
        public string Data { get; set; }
        public CommandTypeEnum CommandType { get; set; }
        public int Seq { get; set; }

        public Command(int seq, CommandTypeEnum commandType, string data)
        {
            this.CommandType = commandType;
            this.Data = data;
            this.Seq = seq;
        }
    }
}
