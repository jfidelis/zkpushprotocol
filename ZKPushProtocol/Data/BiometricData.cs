﻿using NHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class BiometricDataCollect: List<BiometricData>
    {
        public BiometricDataCollect()
        {
        }

        public BiometricDataCollect(HttpRequest httpRequest, string dataBody)
        {
            if (httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {
            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var bioDataList = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_BIODATA)).ToList();

            foreach (var bioData in bioDataList)
            {
                BiometricData biometricData = new BiometricData(bioData);
                base.Add(biometricData);
            }
        }

        public BiometricDataCollect(PushRequestData requestData)
        {
            var bodyData = requestData.GetBodyToString();
            SetData(bodyData);
        }
    }
    public class BiometricData
    {
        public BiometricData()
        {
        }


        public BiometricData(string data)
        {
            if (string.IsNullOrEmpty(data) == false)
            {
                data = data.Replace(Consts.FLD_BIODATA, "");

                var biodataList = data.Split('\t');

                if (biodataList != null && biodataList.Count() > 0)
                {
                    foreach (var biodata in biodataList)
                    {
                        if (biodata.StartsWith(Consts.FLD_PIN))
                        {
                            var pin = biodata.Substring(Consts.FLD_PIN.Length);
                            this.Pin = pin == "" ? 0 : int.Parse(pin);
                        }
                        else if (biodata.StartsWith(Consts.FLD_NO))
                        {
                            var no = biodata.Substring(Consts.FLD_NO.Length);
                            this.No = no == "" ? 0 : int.Parse(no);
                        }
                        else if (biodata.StartsWith(Consts.FLD_INDEX))
                        {
                            var index = biodata.Substring(Consts.FLD_INDEX.Length);
                            this.Index = index == "" ? 0 : int.Parse(index);
                        }
                        else if (biodata.StartsWith(Consts.FLD_VALID))
                        {
                            var valid = biodata.Substring(Consts.FLD_VALID.Length);
                            this.Valid = valid == "" ? false : (int.Parse(valid) == 0 ? false : true);
                        }
                        else if (biodata.StartsWith(Consts.FLD_DURESS))
                        {
                            var duress = biodata.Substring(Consts.FLD_DURESS.Length);
                            this.Duress = duress == "" ? false : (int.Parse(duress) == 0 ? false : true);
                        }
                        else if (biodata.StartsWith(Consts.FLD_TYPE))
                        {
                            var type = biodata.Substring(Consts.FLD_TYPE.Length);
                            this.Type = type == "" ? 0 : int.Parse(type);

                            if (this.Type > Enum.GetValues(typeof(BiopetricDataTypeEnum)).Cast<int>().Min() &&
                                this.Type <= Enum.GetValues(typeof(BiopetricDataTypeEnum)).Cast<int>().Max())
                            {
                                this.BiopetricDataType = (BiopetricDataTypeEnum)this.Type;
                            }
                            else
                            {
                                this.BiopetricDataType = BiopetricDataTypeEnum.NotSupported; 
                            }
                        }
                        else if (biodata.StartsWith(Consts.FLD_MAJORVER))
                        {
                            var majorver = biodata.Substring(Consts.FLD_MAJORVER.Length);
                            this.MajorVerion = majorver == "" ? 0 : int.Parse(majorver);
                        }
                        else if (biodata.StartsWith(Consts.FLD_MINORVER))
                        {
                            var minorver = biodata.Substring(Consts.FLD_MINORVER.Length);
                            this.MinorVersion = minorver == "" ? 0 : int.Parse(minorver);
                        }
                        else if (biodata.StartsWith(Consts.FLD_FORMAT))
                        {
                            var format = biodata.Substring(Consts.FLD_FORMAT.Length);
                            this.Format = format == "" ? 0 : int.Parse(format);
                        }
                        else if (biodata.StartsWith(Consts.FLD_TMP))
                        {
                            var template = biodata.Substring(Consts.FLD_TMP.Length);
                            this.Template = template;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// user employee number. 
        /// </summary>
        public int Pin { get; set; }
        /// <summary>
        /// Biological individual number, default value is 0 
        /// </summary>
        public int No { get; set; }
        /// <summary>
        ///  Biological individual template number, such as one finger to store multiple templates. Start from 0. 
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        ///  Whether the label is valid, 0: invalid, 1: valid, default is 1
        /// </summary>
        public bool Valid { get; set; }
        /// <summary>
        /// whether is it coercion or not, 0: non coercion, 1: coercion, default is 0. 
        /// </summary>
        public bool Duress { get; set; }
        /// <summary>
        /// Biometric types             
        /// value meanings:
        /// 0        universal
        /// 1        fingerprint
        /// 2        face             
        /// 3        voiceprint             
        /// 4        iris             
        /// 5        retina  
        /// 6        palm print             
        /// 7        finger vein             
        /// 8        palm             
        /// 9        light visible face 
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public int Type { get; set; }
        /// <summary>
        ///  main version number, for example: fingerprint algorithm version 10.3, main version is 10, sub version is 3
        ///  【fingerprint】9.0、10.3 and 12.0 
        ///  【finger vein】 3.0   
        ///  【face】 5.0、7.0 and 8.0 
        ///  【palm】1.0 
        /// </summary>
        public BiopetricDataTypeEnum BiopetricDataType { get; set; }
        public int MajorVerion { get; set; }
        /// <summary>
        /// sub version number, for example: fingerprint algorithm version 10.3, main version is 10, sub version is 3. 
        /// 【fingerprint】9.0、10.3 and 12.0 
        /// 【finger vein】3.0 
        /// 【face】5.0、7.0 and 8.0 
        /// 【palm】1.0pin=${XXX}: name of the attendance photo 
        /// </summary>
        public int MinorVersion { get; set; }
        /// <summary>
        /// format of the template, for example fingerprint has ZK\ISO\ANSI and so on. 
        /// 【fingerprint】 value    format                 
        ///     0    ZK                 
        ///     1    ISO                 
        ///     2    ANSI             
        /// 
        /// 【finger vein】 value    format                 
        ///     0    ZK             
        ///     
        /// 【face】        value    format                 
        ///     0    ZK             
        ///     
        /// 【palm】        value    format                 
        ///     0    ZK 
        /// </summary>
        public int Format { get; set; }
        /// <summary>
        ///  data template, need to base64 coding to original binary fingerprint template
        /// </summary>
        public string Template { get; set; }

    }
}
