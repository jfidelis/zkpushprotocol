﻿using NHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class FaceData
    {
        // biophoto pin=1	filename=1.jpg	type=9	size=309140	content=/9j/...
        public FaceData()
        {

        }


        public FaceData(HttpRequest httpRequest, string dataBody)
        {
            if (httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {
            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var data = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_BIOPHOTO)).ToList();

            if (data != null && data.Count > 0)
            {
                data[0] = data[0].Replace(Consts.FLD_BIOPHOTO + " ", "");

                var splitData = data[0].Split('\t');

                if (splitData != null && splitData.Count() > 0)
                {
                    string aux = string.Empty;

                    foreach (var sdParam in splitData)
                    {
                        if (sdParam.StartsWith(Consts.FLD_PIN))
                        {
                            aux = sdParam.Substring(Consts.FLD_PIN.Length);
                            this.Pin = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_FILENAME))
                        {
                            aux = sdParam.Substring(Consts.FLD_FILENAME.Length);
                            this.FileName = aux;
                        }
                        else if (sdParam.StartsWith(Consts.FLD_TYPE))
                        {
                            aux = sdParam.Substring(Consts.FLD_TYPE.Length);
                            this.Type = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_SIZE))
                        {
                            aux = sdParam.Substring(Consts.FLD_SIZE.Length);
                            this.Size = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_CONTENT))
                        {
                            aux = sdParam.Substring(Consts.FLD_CONTENT.Length);
                            this.Content = aux;
                        }
                    }
                }
            }
        }
        public FaceData(PushRequestData requestData)
        {
            if (requestData.Headers.ContainsKey("Content-Length"))
            {
                var bodyData = requestData.GetBodyToString();

                SetData(bodyData);
            }
        }

        public int Pin { get; set; }
        public string FileName { get; set; }
        public int Type { get; set; }
        public int Size { get; set; }
        public string Content { get; set; }
    }
}
