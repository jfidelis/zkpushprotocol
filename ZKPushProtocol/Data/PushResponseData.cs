﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class PushResponseData
    {
        public Dictionary<string, string> Headers { get; set; }
        public string Version = "HTTP/1.1";
        public string StatusCode { get; set; }
        public string BodyData { get; set; }
        public PushResponseData()
        {
            Headers = new Dictionary<string, string>();
        }

        public override string ToString()
        {
            StringBuilder httpStr = new StringBuilder();

            httpStr.AppendLine(Version + " " + StatusCode);

            if (Headers != null && Headers.Count > 0)
            {
                foreach (var header in Headers)
                {
                    httpStr.AppendLine(header.Key + ": " + header.Value);
                }

                httpStr.AppendLine();

            }

            if (string.IsNullOrEmpty(BodyData) == false)
            {
                httpStr.Append(BodyData);
            }

            //httpStr.AppendLine();

            return httpStr.ToString();
        }
    }
}
