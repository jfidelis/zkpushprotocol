﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class PushRequestData
    {
        public string Fragment { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public string Method { get; set; }
        public string QueryString { get; set; }
        public string Uri { get; set; }
        public ArraySegment<byte> BodyData { get; set; }
        public string EndPointClient { get; set; }

        public PushRequestData()
        {
            Headers = new Dictionary<string, string>();
            BodyData = new ArraySegment<byte>();
        }

        private Dictionary<string, string> GetParams(string data)
        {
            Dictionary<string, string> res = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(data) == false)
            {
                var items = data.Split(new char[] { '&' });

                if (items != null)
                {

                    foreach (var item in items)
                    {
                        var posID = item.IndexOf("ID=");
                        if (posID > -1)
                        {
                            string idStr = item.Substring(posID);
                            var id = idStr.Split(new char[] { '=' });
                            if(res.ContainsKey(id[0]) == false)
                                res.Add(id[0], id[1]);
                        }
                        else
                        {
                            var d = item.Split(new char[] { '=' });
                            if (d != null)
                            {
                                if (res.ContainsKey(d[0]) == false)
                                    res.Add(d[0], d[1]);
                            }
                        }
                    }
                }
            }
            return res;
        }

        public Dictionary<string, string> GetQueryStringParams()
        {
            return GetParams(QueryString);
        }

        public Dictionary<string, string> GetParamsInBody()
        {
            Encoding encoding = Encoding.UTF8;
            string dataBody = (BodyData.Array != null && BodyData.Array.Count() > 0) ? encoding.GetString(BodyData.Array) : string.Empty;
            return GetParams(dataBody);
        }

        public string GetBodyToString()
        {
            Encoding encoding = Encoding.UTF8;
            string dataBody = (BodyData.Array != null && BodyData.Array.Count() > 0) ? encoding.GetString(BodyData.Array) : string.Empty;
            return dataBody;
        }

        public PushApiType GetApiType()
        {
            PushApiType pushApi = PushApiType.none;

            if (Uri.StartsWith("/iclock/cdata"))
                pushApi = PushApiType.cdata;

            else if (Uri.StartsWith("/iclock/getrequest"))
                pushApi = PushApiType.getrequest;

            else if (Uri.StartsWith("/iclock/rtdata"))
                pushApi = PushApiType.rtdata;

            else if (Uri.StartsWith("/iclock/registry"))
                pushApi = PushApiType.registry;

            else if (Uri.StartsWith("/iclock/push"))
                pushApi = PushApiType.push;

            else if (Uri.StartsWith("/iclock/devicecmd"))
                pushApi = PushApiType.devicecmd;

            else if (Uri.StartsWith("/iclock/querydata"))
                pushApi = PushApiType.querydata;

            else if (Uri.StartsWith("/iclock/ping"))
                pushApi = PushApiType.ping;

                return pushApi;
        }
    }
}
