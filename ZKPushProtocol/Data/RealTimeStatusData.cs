﻿using NHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class RealTimeStatusData
    {

        public RealTimeStatusData()
        {
        }

        public RealTimeStatusData(HttpRequest httpRequest, string dataBody)
        {
            if (httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {
            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var data = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_TIMERT)).ToList();

            if (data != null && data.Count > 0)
            {
                var splitData = data[0].Split('\t');

                if (splitData != null && splitData.Count() > 0)
                {
                    string aux = string.Empty;

                    foreach (var sdParam in splitData)
                    {
                        if (sdParam.StartsWith(Consts.FLD_TIMERT))
                        {
                            aux = sdParam.Substring(Consts.FLD_TIMERT.Length);
                            this.DateTimeStatus = aux == "" ? DateTime.Now : Convert.ToDateTime(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_SENSOR))
                        {
                            aux = sdParam.Substring(Consts.FLD_SENSOR.Length);
                            this.Sensor = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_RELAY))
                        {
                            aux = sdParam.Substring(Consts.FLD_RELAY.Length);
                            this.Relay = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_DOOR))
                        {
                            aux = sdParam.Substring(Consts.FLD_DOOR.Length);
                            this.Door = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_ALARM))
                        {
                            aux = sdParam.Substring(Consts.FLD_ALARM.Length);
                            this.Alarm = aux;
                        }
                    }
                }
            }
        }

        public RealTimeStatusData(PushRequestData requestData)
        {
            if (requestData.Headers.ContainsKey("Content-Length"))
            {
                var bodyData = requestData.GetBodyToString();
                SetData(bodyData);
            }
        }

        public DateTime DateTimeStatus { get; set; }
        public int Sensor { get; set; }
        public int Relay { get; set; }
        public string Alarm { get; set; }
        public int Door { get; set; }
    }
}
