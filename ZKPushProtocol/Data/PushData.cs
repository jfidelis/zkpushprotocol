﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class PushData
    {
        private string serverVersion = "3.0.1";
        private string serverName = "ADMS";
        private string pushVersion = "3.0.1";
        private int errorDelay = 30;
        private string transTimes = "00:0014:00";
        private int requestDelay = 1;
        private int transInterval = 1;
        private int realTime = 1;
        private string transTables = "User Transaction Facev7 templatev10 biodata";
        private int timeZone = -3;
        private int timeoutSec = 10;

        public string ServerVersion
        {
            get { return serverVersion; }
            set { serverVersion = value; }
        }
        public string ServerName
        {
            get { return serverName; }
            set { serverName = value; }
        }

        public string PushVersion
        {
            get { return pushVersion; }
            set { pushVersion = value; }
        }

        public int ErrorDelay
        {
            get { return errorDelay; }
            set { errorDelay = value; }
        }

        public string TransTimes
        {
            get { return transTimes; }
            set { transTimes = value; }
        }

        public int RequestDelay
        {
            get { return requestDelay; }
            set { requestDelay = value; }
        }

        public int TransInterval
        {
            get { return transInterval; }
            set { transInterval = value; }
        }

        public int RealTime
        {
            get { return realTime; }
            set { realTime = value; }
        }

        public string TransTables
        {
            get { return transTables; }
            set { transTables = value; }
        }

        public int TimeZone
        {
            get { return timeZone; }
            set { timeZone = value; }
        }

        public int TimeoutSec
        {
            get { return timeoutSec; }
            set { timeoutSec = value; }
        }
    }
}
