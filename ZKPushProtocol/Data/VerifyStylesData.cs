﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class VerifyStylesData
    {
        public VerifyStylesData()
        {
        }
        public VerifyStylesData(string dataHex)
        {
            // | 0 | Finger vein, face, fingerprint or password(automatic recognition) | 
            // | 1 | Only fingerprint | 
            // | 2 | Employee number verification | 
            // | 3 | Only password | | 4 | Only card |
            // | 5 | fingerprint or password |
            // | 6 | Fingerprint or card |
            // | 7 | Card or password | 
            // | 8 | Employee number and fingerprint | 
            // | 9 | Fingerprint and password | 
            // | 10 | Card and fingerprint | 
            // | 11 | Card and password | 
            // | 12 | Fingerprint and password, card |
            // | 13 | Employee number and fingerprint, password |
            // | 14 | Employee number and fingerprint or Card and fingerprint |
            // | 15 | Face |
            // | 16 | Face and fingerprint |
            // | 17 | Face and password |
            // | 18 | Face and card | 
            // | 19 | Face and fingerprint, card | 
            // | 20 | face and fingerprint, password |
            // | 21 | Finger vein|
            // | 22 | Finger vein and password| 
            // | 23 | Finger vein and card |
            // | 24 | Finger vein and password, card | | 

            var binaryString = Convert.ToString(Convert.ToInt64(dataHex, 16), 2);

            if(binaryString.Length > 22)
            {
                this.VerifyStyles = new Dictionary<VerifyTypeEnum, bool>();
                for(int i = 0; i < binaryString.Length; i ++)
                {
                    this.VerifyStyles.Add((VerifyTypeEnum)i, binaryString[i] == '1' ? true : false);
                }
            }

        }

        public Dictionary<VerifyTypeEnum, bool> VerifyStyles { get; set; }

        private string BinaryStringToHexString(string binary)
        {
            StringBuilder result = new StringBuilder(binary.Length / 8 + 1);

            int mod4Len = binary.Length % 8;
            if (mod4Len != 0)
            {
                binary = binary.PadLeft(((binary.Length / 8) + 1) * 8, '0');
            }

            for (int i = 0; i < binary.Length; i += 8)
            {
                string eightBits = binary.Substring(i, 8);
                result.AppendFormat("{0:x2}", Convert.ToByte(eightBits, 2));
            }

            return result.ToString();
        }

        public string GetDataHex()
        {
            string data = string.Empty;
            for (int i = 0; i < 28; i++)
            {
                if (VerifyStyles != null && VerifyStyles.ContainsKey((VerifyTypeEnum)i))
                {
                    data += VerifyStyles[(VerifyTypeEnum)i] ? "1" : "0";
                }
                else
                {
                    data += "0";
                }
            }

            data = BinaryStringToHexString(data);

            return data;
        }

    }
}
