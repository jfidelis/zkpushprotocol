﻿using NHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class RealTimeEventData
    {
        //time=2019-12-10 19:20:16	pin=4	cardno=0	eventaddr=1	event=0	inoutstatus=1	verifytype=0	index=271	sitecode=0	linkid=0
        public RealTimeEventData()
        {
        }

        public RealTimeEventData(HttpRequest httpRequest, string dataBody)
        {
            if(httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {

            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var data = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_TIMERT)).ToList();

            if (data != null && data.Count > 0)
            {
                var splitData = data[0].Split('\t');

                if (splitData != null && splitData.Count() > 0)
                {
                    string aux = string.Empty;

                    foreach (var sdParam in splitData)
                    {
                        if (sdParam.StartsWith(Consts.FLD_CARDNO))
                        {
                            aux = sdParam.Substring(Consts.FLD_CARDNO.Length);
                            this.CardNumber = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_PIN))
                        {
                            aux = sdParam.Substring(Consts.FLD_PIN.Length);
                            this.Pin = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_TIMERT))
                        {
                            aux = sdParam.Substring(Consts.FLD_TIMERT.Length);
                            this.DateTimeAccess = aux == "" ? DateTime.Now : Convert.ToDateTime(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_INDEX))
                        {
                            aux = sdParam.Substring(Consts.FLD_INDEX.Length);
                            this.Index = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_EVENT))
                        {
                            aux = sdParam.Substring(Consts.FLD_EVENT.Length);
                            this.Event = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_EVENTADDR))
                        {
                            aux = sdParam.Substring(Consts.FLD_EVENTADDR.Length);
                            this.EventAddr = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_INOUTSTATUS))
                        {
                            aux = sdParam.Substring(Consts.FLD_INOUTSTATUS.Length);
                            this.InOutStatus = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_LINKID))
                        {
                            aux = sdParam.Substring(Consts.FLD_LINKID.Length);
                            this.LinkId = aux == "" ? 0 : int.Parse(aux);
                        }
                        else if (sdParam.StartsWith(Consts.FLD_SITECODE))
                        {
                            aux = sdParam.Substring(Consts.FLD_SITECODE.Length);
                            this.SiteCode = aux == "" ? 0 : int.Parse(aux);
                        }
                    }
                }
            }
        }

        public RealTimeEventData(PushRequestData requestData)
        {
            if (requestData.Headers.ContainsKey("Content-Length"))
            {
                var bodyData = requestData.GetBodyToString();
                SetData(bodyData);
            }
        }

        public DateTime DateTimeAccess { get; set; }
        public int Pin { get; set; }
        public int CardNumber { get; set; }
        public int EventAddr { get; set; }
        public int Event { get; set; }
        public int InOutStatus { get; set; }
        public int VerifyType { get; set; }
        public int Index { get; set; }
        public int SiteCode { get; set; }
        public int LinkId { get; set; }
    }
}
