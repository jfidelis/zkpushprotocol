﻿using NHttp;
using System;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class PersonData
    {
        public PersonData()
        {
        }

        public PersonData(HttpRequest httpRequest, string dataBody)
        {
            if (httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {
            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var userData = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_USER_UID)).ToList();

            if (userData != null && userData.Count > 0)
            {
                var splitUserData = userData[0].Split('\t');

                if (splitUserData != null && splitUserData.Count() > 0)
                {
                    foreach (var userParam in splitUserData)
                    {
                        if (userParam.StartsWith(Consts.FLD_CARDNO))
                        {
                            var cardNo = userParam.Substring(Consts.FLD_CARDNO.Length);
                            this.CardNumber = cardNo == "" ? 0 : int.Parse(cardNo);
                        }
                        else if (userParam.StartsWith(Consts.FLD_USER_UID))
                        {
                            var userUid = userParam.Substring(Consts.FLD_USER_UID.Length);
                            this.UserUID = userUid == "" ? 0 : int.Parse(userUid);
                        }
                        else if (userParam.StartsWith(Consts.FLD_PIN))
                        {
                            var pin = userParam.Substring(Consts.FLD_PIN.Length);
                            this.Pin = pin == "" ? 0 : int.Parse(pin);
                        }
                        else if (userParam.StartsWith(Consts.FLD_GROUP))
                        {
                            var group = userParam.Substring(Consts.FLD_GROUP.Length);
                            this.Group = group == "" ? 0 : int.Parse(group);
                        }
                        else if (userParam.StartsWith(Consts.FLD_PRIVILEGE))
                        {
                            var privilege = userParam.Substring(Consts.FLD_PRIVILEGE.Length);
                            this.Privilege = privilege == "" ? PrivilegeTypeEnum.Normal : (PrivilegeTypeEnum)int.Parse(privilege);
                        }
                        else if (userParam.StartsWith(Consts.FLD_DISABLE))
                        {
                            var disable = userParam.Substring(Consts.FLD_DISABLE.Length);
                            this.Disable = disable == "" ? false : (disable == "0" ? false : true);
                        }
                        else if (userParam.StartsWith(Consts.FLD_VERIFY))
                        {
                            var verify = userParam.Substring(Consts.FLD_VERIFY.Length);
                            this.Verify = verify == "" ? 0 : int.Parse(verify);
                        }
                        else if (userParam.StartsWith(Consts.FLD_ENDTIME))
                        {
                            var endtime = userParam.Substring(Consts.FLD_ENDTIME.Length);
                            this.EndTime = endtime == "" ? 0 : int.Parse(endtime);
                        }
                        else if (userParam.StartsWith(Consts.FLD_PASSWORD))
                        {
                            this.PassWord = userParam.Substring(Consts.FLD_PASSWORD.Length);
                        }
                        else if (userParam.StartsWith(Consts.FLD_NAME))
                        {
                            this.Name = userParam.Substring(Consts.FLD_NAME.Length);
                        }
                    }
                }
            }
        }
        public PersonData(PushRequestData requestData)
        {
            if (requestData.Headers.ContainsKey("Content-Length"))
            {
                var bodyData = requestData.GetBodyToString();

                SetData(bodyData);
            }
        }

        public int UserUID { get; set; }
        public bool Disable { get; set; }
        public int Verify { get; set; }
        public int CardNumber { get; set; }
        public int Pin { get; set; }
        public string PassWord { get; set; }
        /// <summary>
        /// If DateFmrFunOn = 1 using function oldDateTime, else if 0, using YYYYMMDD
        /// </summary>
        public int StartTime { get; set; }
        /// <summary>
        /// If DateFmrFunOn = 1 using function oldDateTime, else if 0, using YYYYMMDD
        /// </summary>
        public int EndTime { get; set; }

        public PrivilegeTypeEnum Privilege { get; set; }

        public int Group { get; set; }

        public string Name { get; set; }


    }
}
