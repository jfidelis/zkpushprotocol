﻿using NHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class ConfigData
    {
        public ConfigData()
        {
        }

        public ConfigData(HttpRequest httpRequest, string dataBody)
        {
            if (httpRequest.ContentLength > 0)
            {
                SetData(dataBody);
            }
        }

        private void SetData(string bodyData)
        {
            var splitBody = bodyData.Split(new char[] { '\r', '\n' });

            var configData = splitBody.Where(x => x != "" && x.StartsWith(Consts.FLD_DEVICENAME)).ToList();

            if (configData?.Count > 0)
            {
                var splitConfigData = configData[0].Split(',');
                if (splitConfigData?.Count() > 0)
                {
                    foreach (var p in splitConfigData)
                    {
                        if (p.StartsWith(Consts.FLD_DEVICENAME))
                        {
                            var deviceName = p.Substring(Consts.FLD_DEVICENAME.Length);
                            this.DeviceName = deviceName;
                        }
                        else if (p.StartsWith(Consts.FLD_FIRMVER))
                        {
                            var firmVer = p.Substring(Consts.FLD_FIRMVER.Length);
                            this.FirmVersion = firmVer;
                        }
                        else if (p.StartsWith(Consts.FLD_IPADDRESS))
                        {
                            var ipAddress = p.Substring(Consts.FLD_IPADDRESS.Length);
                            this.IPAddress = ipAddress;
                        }
                        else if (p.StartsWith(Consts.FLD_NETMASK))
                        {
                            var netMask = p.Substring(Consts.FLD_NETMASK.Length);
                            this.NetMask = netMask;
                        }
                        else if (p.StartsWith(Consts.FLD_GATEADDRESS))
                        {
                            var gateAddress = p.Substring(Consts.FLD_GATEADDRESS.Length);
                            this.GATEIPAddress = gateAddress;
                        }
                        else if (p.StartsWith(Consts.FLD_MAXUSERCOUNT))
                        {
                            var maxUserCount = p.Substring(Consts.FLD_MAXUSERCOUNT.Length);
                            this.MaxUserCount = maxUserCount == "" ? 0 : int.Parse(maxUserCount);
                        }
                        else if (p.StartsWith(Consts.FLD_MAXATTLOGCOUNT))
                        {
                            var maxAttLogCount = p.Substring(Consts.FLD_MAXATTLOGCOUNT.Length);
                            this.MaxAttLogCount = maxAttLogCount == "" ? 0 : int.Parse(maxAttLogCount);
                        }
                        else if (p.StartsWith(Consts.FLD_MAXUSERFINGERCOUNT))
                        {
                            var maxUserFpCount = p.Substring(Consts.FLD_MAXUSERFINGERCOUNT.Length);
                            this.MaxUserFingerCount = maxUserFpCount == "" ? 0 : int.Parse(maxUserFpCount);
                        }
                        else if (p.StartsWith(Consts.FLD_MAXFINGERCOUNT))
                        {
                            var maxFpCount = p.Substring(Consts.FLD_MAXFINGERCOUNT.Length);
                            this.MaxFingerCount = maxFpCount == "" ? 0 : int.Parse(maxFpCount);
                        }
                        else if (p.StartsWith(Consts.FLD_MAXBIOPHOTOCOUNT))
                        {
                            var maxBioPhotoCount = p.Substring(Consts.FLD_MAXBIOPHOTOCOUNT.Length);
                            this.MaxBioPhotoCount = maxBioPhotoCount == "" ? 0 : int.Parse(maxBioPhotoCount);
                        }
                        else if (p.StartsWith(Consts.FLD_SIMPLEEVENTTYPE))
                        {
                            var simpleEventType = p.Substring(Consts.FLD_SIMPLEEVENTTYPE.Length);
                            this.SimpleEventType = simpleEventType == "" ? 0 : int.Parse(simpleEventType);
                        }
                        else if (p.StartsWith(Consts.FLD_VERIFYSTYLES))
                        {
                            var verifyStyles = p.Substring(Consts.FLD_VERIFYSTYLES.Length);
                            this.VerifyStyles = verifyStyles;
                        }
                        else if (p.StartsWith(Consts.FLD_EVENTTYPES))
                        {
                            var eventTypes = p.Substring(Consts.FLD_EVENTTYPES.Length);
                            this.EventTypes = eventTypes.TrimEnd(new char[] { '\0' });
                        }
                        else if (p.StartsWith(Consts.FLD_AUTOSERVERFUNON))
                        {
                            var autoServerFunOn = p.Substring(Consts.FLD_AUTOSERVERFUNON.Length);
                            this.AutoServerFunOn = autoServerFunOn == "" ? 0 : int.Parse(autoServerFunOn);
                        }
                        else if (p.StartsWith(Consts.FLD_AUTOSERVERMODE))
                        {
                            var autoServerMode = p.Substring(Consts.FLD_AUTOSERVERMODE.Length);
                            this.AutoServerMode = autoServerMode == "" ? 0 : int.Parse(autoServerMode);
                        }
                        else if (p.StartsWith(Consts.FLD_MULTICARDINTERTIMEFUNON))
                        {
                            var multiCardInterTimeFunOn = p.Substring(Consts.FLD_MULTICARDINTERTIMEFUNON.Length);
                            this.MultiCardInterTimeFunOn = multiCardInterTimeFunOn == "" ? 0 : int.Parse(multiCardInterTimeFunOn);
                        }
                    }
                }
            }
        }

        public ConfigData(PushRequestData requestData)
        {
            if (requestData.Headers.ContainsKey("Content-Length") && requestData.Headers["Content-Length"] != "0")
            {
                var bodyData = requestData.GetBodyToString();
                SetData(bodyData);
            }
        }
        #region Device Info

        public string DeviceName { get; protected set; }
        public string FirmVersion { get; protected set; }

        #endregion

        #region Network

        public string IPAddress { get; protected set; }
        public string NetMask { get; protected set; }
        public string GATEIPAddress { get; protected set; }

        #endregion

        #region Capatity

        public int MaxUserCount { get; protected set; }
        public int MaxAttLogCount { get; protected set; }
        public int MaxUserFingerCount { get; protected set; }
        public int MaxFingerCount { get; protected set; }
        public int MaxBioPhotoCount { get; protected set; }

        #endregion

        #region Access Controls

        public int SimpleEventType { get; set; }
        public string VerifyStyles { get; set; }
        public string EventTypes { get; set; }
        public VerifyStylesData VerifyStylesData { get; set; }

        #endregion

        #region Inquire Access

        public int AutoServerFunOn { get; set; }
        public int AutoServerMode { get; set; }

        #endregion

        public int MultiCardInterTimeFunOn { get; set; }

    }
}
