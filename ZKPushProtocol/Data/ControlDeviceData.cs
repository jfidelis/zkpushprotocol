﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZKPushProtocol
{
    public class ControlDeviceData
    {
        public int Output { get; set; }
        public int Door { get; set; }
        public int Lock { get; set; }
        public int TimerOpenSeg { get; set; }

        public string GetCommand()
        {
            return string.Format("{0:00}{1:00}{2:00}{3:00}", Output, Door, Lock, TimerOpenSeg);
        }
    }
}
