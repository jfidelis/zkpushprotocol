﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZKPushProtocol;
using ZKPushProtocol.Http;

namespace AppPush
{
    public partial class frmZKPushApp : Form
    {
        public frmZKPushApp()
        {
            InitializeComponent();
        }

        //Server server = null;
        HttpServerParser server = null;

        private System.Threading.Timer pollingTimer;
        List<DeviceContext> configs = new List<DeviceContext>();

        private delegate void SafeCallDelegate(string text);
        private delegate void SafeCallDelegateListViewItem(ListViewItem item);

        private void Form1_Load(object sender, EventArgs e)
        {
            configs.Add(new DeviceContext() { Id = 1, SerialNumber = "CKJF202260257", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 2, SerialNumber = "CGFE193460002", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 3, SerialNumber = "CGFE193460003", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 4, SerialNumber = "CGFE193460004", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 5, SerialNumber = "CGFE193460005", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 6, SerialNumber = "CGFE193460006", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });
            //configs.Add(new DeviceContext() { Id = 7, SerialNumber = "CGFE193460007", Configs = new PushData() { TimeZone = 0 }, KeepAliveTimeout = new System.Diagnostics.Stopwatch() });

            cmbIndex.SelectedIndex = 0;
            cmbType.SelectedIndex = 1;

            foreach (var c in configs)
            {
                c.KeepAliveTimeout.Start();
            }

            pollingTimer = new System.Threading.Timer(PollingTimerCallback, null, 10000, 10000);
        }

        private void WriteTextStatus(string text)
        {
            if (lblStatus.InvokeRequired)
            {
                var d = new SafeCallDelegate(WriteTextStatus);
                lblStatus.Invoke(d, new object[] { text });
            }
            else
            {
                lblStatus.Text = text;
            }
        }

        private void WriteListViewItem(ListViewItem item)
        {
            if (lsvEvents.InvokeRequired)
            {
                var d = new SafeCallDelegateListViewItem(WriteListViewItem);
                lsvEvents.Invoke(d, new object[] { item });
            }
            else
            {
                lsvEvents.Items.Add (item);
            }
        }

        private void Server_onConnectedEvent(int id)
        {
            foreach (var c in configs)
            {
                if(c.Id == id)
                {
                    if(c.Conected == false)
                    {
                        // Informa conectado
                        WriteTextStatus( $"Device Id: {c.Id } NS.:{c.SerialNumber} - On ");

                        ListViewItem log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: {c.Id } NS.:{c.SerialNumber} - On " });
                        WriteListViewItem(log);
                    }
                    c.Conected = true;
                    c.KeepAliveTimeout.Restart();
                }
            }
        }

        private void Server_onCommandStatusEvent(int id, CommandTypeEnum commandType, int status, object data = null)
        {
            foreach (var c in configs)
            {
                if (c.Id == id)
                {
                    ListViewItem log;
                    if (commandType == CommandTypeEnum.GetPerson || 
                        commandType == CommandTypeEnum.GetBiometrics || 
                        commandType == CommandTypeEnum.ConfigData ||
                        commandType == CommandTypeEnum.GetFaceByIdPerson 
                        )
                    {
                        var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                        log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: {c.Id } Cmd.:{commandType} - Status.:{status} - Json:{json}" });
                    }
                    else
                    {
                        if(commandType == CommandTypeEnum.PersonAdd && !string.IsNullOrEmpty(txtIdPerson.Text))
                        {
                            var personId = int.Parse(txtIdPerson.Text);
                            server.SetUserAuthorize(configs[0].Id, personId);
                        }

                        log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: {c.Id } Cmd.:{commandType} - Status.:{status} " });
                    }

                    WriteListViewItem(log);


                    //if (c.Conected == false)
                    //{
                    //    // Informa conectado
                    //    WriteTextStatus($"Device Id: {c.Id } NS.:{c.SerialNumber} - On ");

                    //    ListViewItem log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: {c.Id } NS.:{c.SerialNumber} - On " });
                    //    WriteListViewItem(log);
                    //}
                    //c.Conected = true;
                    //c.KeepAliveTimeout.Restart();
                }
            }
        }

        private void Server_onRealTimeEvent(int id, RealTimeEvTypeEnum typeEnum, object data)
        {
            ListViewItem log;
            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
            log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: { id } {  typeEnum.ToString().ToUpper()  } - Json:{json}" });
            WriteListViewItem(log);
        }

        private void Server_onNewBiometricsEvent(int id, BiometricDataCollect data)
        {
            ListViewItem log;
            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
            log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: { id } - Cad. Nova Biometria - Json:{json}" });
            WriteListViewItem(log); 
        }

        private void Server_onNewFaceEvent(int id, FaceData data)
        {
            ListViewItem log;
            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
            log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: { id } - Cad. Nova Face - Json:{json}" });
            WriteListViewItem(log);
        }


        private void btnWaitConnect_Click(object sender, EventArgs e)
        {
            if (btnWaitConnect.Text == "Aguardar Conexão")
            {
                //server = new Server(configs, txtHost.Text, int.Parse(txtPort.Text));
                server = new HttpServerParser(configs, txtHost.Text, int.Parse(txtPort.Text));
                server.onConnectedEvent += Server_onConnectedEvent;
                server.onCommandStatusEvent += Server_onCommandStatusEvent;
                server.onRealTimeEvent += Server_onRealTimeEvent;
                server.onNewBiometricsEvent += Server_onNewBiometricsEvent;
                server.onNewFaceEvent += Server_onNewFaceEvent;
                server.Start();

                btnWaitConnect.Text = "Encerrar";

            }
            else
            {
                server.Stop();

                btnWaitConnect.Text = "Aguardar Conexão";

                server = null;
            }
        }

        private void PollingTimerCallback(object state)
        {
            pollingTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            try
            {
                foreach (var c in configs)
                {
                    if(c.KeepAliveTimeout.ElapsedMilliseconds > (40 * 1000))
                    {
                        c.KeepAliveTimeout.Reset();
                        c.Conected = false;
                        // Informa desconectado
                        WriteTextStatus( $"Device Id: {c.Id } NS.:{c.SerialNumber} - Off ");

                        ListViewItem log = new ListViewItem(new string[] { DateTime.Now.ToString(), $"Device Id: {c.Id } NS.:{c.SerialNumber} - Off " });
                        WriteListViewItem(log);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                pollingTimer.Change(10000, 10000);
            }
        }

        private void btnSendDateTime_Click(object sender, EventArgs e)
        {
            server.SetDateTime(configs[0].Id, dtpDateTimeSend.Value); 
        }

        private void btnSendPerson_Click(object sender, EventArgs e)
        {
            PersonData person = new PersonData();

            person.CardNumber = int.Parse(txtCard.Text);
            person.Pin = int.Parse(txtIdPerson.Text);
            person.PassWord = txtPassWord.Text;
            person.Group = int.Parse(txtGrupo.Text);
            person.Name = txtNamePerson.Text;
            person.StartTime = 0; //dtValStart.Value;
            person.EndTime = 0; //dtValEnd.Value;
            if (chkSupervisor.Checked)
            {
                person.Privilege = PrivilegeTypeEnum.SuperAdmin;
            }
            else
            {
                person.Privilege = PrivilegeTypeEnum.Normal;
            }

            server.SendPerson(configs[0].Id, person);
        }

        private void btnExcludePerson_Click(object sender, EventArgs e)
        {
            var personId = int.Parse(txtIdPerson.Text);
            server.ExcludePerson(configs[0].Id, personId);
        }

        private void btnGetPerson_Click(object sender, EventArgs e)
        {
            if (chkGetAllDataPersons.Checked)
            {
                server.GetAllPersonsData(configs[0].Id);
            }
            else
            { 
                var personId = int.Parse(txtIdPerson.Text);
                server.GetPerson(configs[0].Id, personId);
            }
        }

        private void btnGetBiometricData_Click(object sender, EventArgs e)
        {
            var personId = int.Parse(txtIdPerson.Text);
            server.GetTemplates(configs[0].Id, personId);
        }

        private void btnCopyList_Click(object sender, EventArgs e)
        {
            StringBuilder clipText = new StringBuilder();
            foreach (ListViewItem item in lsvEvents.Items)
            {
                clipText.AppendLine(item.SubItems[0].Text + item.SubItems[1].Text);
            }
            if (!String.IsNullOrEmpty(clipText.ToString()))
            {
                Clipboard.SetText(clipText.ToString());
            }
        }

        private void btnClearLst_Click(object sender, EventArgs e)
        {
            lsvEvents.Items.Clear();
        }

        //private void btnSendBiometricData_Click(object sender, EventArgs e)
        //{
        //    BiometricDataCollect biometrics = new BiometricDataCollect();

        //    biometrics.Add(
        //        new BiometricData()
        //        {
        //            Template = txtFingerPrintToSend.Text,
        //            Index = 1,
        //            Pin = int.Parse(txtIdPerson.Text)
        //        }
        //        );

        //    server.SendBiometricDataCollect(configs[0].Id, biometrics);
        //}

        private void btnSendFingerPrint_Click(object sender, EventArgs e)
        {
            if((BiopetricDataTypeEnum)cmbType.SelectedIndex == BiopetricDataTypeEnum.Palm)
            {
                var biometricDataCollect = JsonConvert.DeserializeObject<BiometricDataCollect>(txtFingerPrintToSend.Text);

                server.SendBiometricDataCollect(configs[0].Id, biometricDataCollect);
            }
            else
            {
                var bio = new BiometricData()
                {
                    Template = txtFingerPrintToSend.Text,
                    Index = cmbIndex.SelectedIndex,
                    Pin = int.Parse(txtIdPerson.Text),
                    //BiopetricDataType = BiopetricDataTypeEnum.Fingerprint,
                    BiopetricDataType = (BiopetricDataTypeEnum)cmbType.SelectedIndex,
                    Duress = false,
                    Format = 0,
                    MajorVerion = 12,
                    MinorVersion = 0,
                    // [0 - 9] for fingerprint (os dedos das duas mãos) and [0 - 1] palm (mão direita e esquerda) 
                    No = 0,
                    //Type = (int)BiopetricDataTypeEnum.Fingerprint,
                    Type = (int)cmbType.SelectedIndex,
                    Valid = true
                };

                server.SendBiometricData(configs[0].Id, bio);
            }
        }

        private void btnSendFace_Click(object sender, EventArgs e)
        {
            /*
            var bio = new BiometricData()
            {
                Template = txtFaceToSend.Text,
                Index = 0,
                Pin = int.Parse(txtIdPerson.Text),
                BiopetricDataType = BiopetricDataTypeEnum.LightVisibleFace,
                Duress = false,
                Format = 0,
                MajorVerion = 5,
                MinorVersion = 6,
                No = 0,
                Type = (int)BiopetricDataTypeEnum.LightVisibleFace,
                Valid = true
            };

            server.SendBiometricData(configs[0].Id, bio);
            */

            var face = new FaceData()
            {
                Pin = int.Parse(txtIdPerson.Text),
                Size = txtFaceToSend.Text.Length,
                Content = txtFaceToSend.Text,
                FileName = "1.jpg",
                Type = (int)BiopetricDataTypeEnum.LightVisibleFace
            };

            server.SendFaceData(configs[0].Id, face);

        }

        private void btnConfigGet_Click(object sender, EventArgs e)
        {
            server.GetConfigs(configs[0].Id);
        }

        private void btnConfigSend_Click(object sender, EventArgs e)
        {
            ConfigData config = new ConfigData();
            config.SimpleEventType = 1;
            config.VerifyStylesData = new VerifyStylesData();
            config.VerifyStylesData.VerifyStyles = new Dictionary<VerifyTypeEnum, bool>();
            config.VerifyStylesData.VerifyStyles.Add(VerifyTypeEnum.Face, true);
            config.EventTypes = "BF0FE03D30000100000000007000000000000000000000000077402001000000";
            server.SendConfigs(configs[0].Id, config);
        }

        private void btnGetFaceByIdPerson_Click(object sender, EventArgs e)
        {
            var personId = int.Parse(txtIdPerson.Text);
            server.GetFaceByIdPerson(configs[0].Id, personId);
        }

        private void btnGetDateTime_Click(object sender, EventArgs e)
        {
            server.GetDateTime(configs[0].Id);
        }

        private void BtnOpenDoor_Click(object sender, EventArgs e)
        {
            ControlDeviceData controlDeviceData = new ControlDeviceData();
            controlDeviceData.Door = 1;
            controlDeviceData.Lock = 1;
            controlDeviceData.Output = 1;
            controlDeviceData.TimerOpenSeg = 5;
            server.SendControlDevice(configs[0].Id, controlDeviceData);
        }

        private void btnSendAutoServerFun_Click(object sender, EventArgs e)
        {
            server.SetAutoServerFun(configs[0].Id, chkAutoServerFun.Checked);
        }

        private void btnFaceToImg_Click(object sender, EventArgs e)
        {
            ofdFindFile.FileName = "face.jpg";
            ofdFindFile.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (ofdFindFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filename = ofdFindFile.FileName;
                    picPhotoFace.Image = new Bitmap(filename);

                    using (Image image = Image.FromFile(filename))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, image.RawFormat);
                            byte[] imageBytes = m.ToArray();

                            string base64String = Convert.ToBase64String(imageBytes);

                            byte[] imageResBytes = AlterImageRes(imageBytes, 120, 140);

                            string base64StringRes = Convert.ToBase64String(imageResBytes);

                            //txtFaceToSend.Text = base64String;

                            txtFaceToSend.Text = base64StringRes;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }

        /// <summary>
        /// Resizes the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="size">The size.</param>
        /// <param name="preserveAspectRatio">if set to <c>true</c> [preserve aspect ratio].</param>
        /// <returns></returns>
        private System.Drawing.Image ResizeImage(System.Drawing.Image image, Size size,
            bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = image.Width;
                int originalHeight = image.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            System.Drawing.Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        }

        /// <summary>
        /// Alters the image resource.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        private byte[] AlterImageRes(byte[] data, int width, int height)
        {
            using (var ms = new MemoryStream(data, 0, data.Length))
            {
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                System.Drawing.Image resized = ResizeImage(image, new Size(width, height), false);

                using (MemoryStream stream = new MemoryStream())
                {
                    resized.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] newData = stream.ToArray();
                    // var _base64String  = "data:image/jpg;base64," + Convert.ToBase64String(newData);
                    return newData;
                }
            }
        }

        private void btnAutoServerMode_Click(object sender, EventArgs e)
        {
            server.SetAutoServerMode(configs[0].Id, chkAutoServerMode.Checked ? 1 : 0);
        }

        private void btnSendMultiCardOn_Click(object sender, EventArgs e)
        {
            server.SetMultiCardInterTimeFunOn(configs[0].Id, chkMultiCardOn.Checked);
        }

        private void btnVarBynaryToImgAndFace_Click(object sender, EventArgs e)
        {
            ofdFindFile.FileName = "facevarbinary.txt";
            ofdFindFile.Filter = "VarBinary Files(*.txt)|*.txt";
            if (ofdFindFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var filename = ofdFindFile.FileName;


                    //picPhotoFace.Image = new Bitmap(filename);

                    //using (Image image = Image.FromFile(filename))
                    //{
                    //    using (MemoryStream m = new MemoryStream())
                    //    {
                    //        image.Save(m, image.RawFormat);
                    //        byte[] imageBytes = m.ToArray();

                    //        string base64String = Convert.ToBase64String(imageBytes);

                    //        byte[] imageResBytes = AlterImageRes(imageBytes, 120, 140);

                    //        string base64StringRes = Convert.ToBase64String(imageResBytes);

                    //        //txtFaceToSend.Text = base64String;

                    //        txtFaceToSend.Text = base64StringRes;
                    //    }
                    //}

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error.\n\nError message: {ex.Message}\n\n" +
                    $"Details:\n\n{ex.StackTrace}");
                }
            }
        }
    }
}
