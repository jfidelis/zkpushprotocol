﻿namespace AppPush
{
    partial class frmZKPushApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWaitConnect = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.grpDateTimeSend = new System.Windows.Forms.GroupBox();
            this.btnGetDateTime = new System.Windows.Forms.Button();
            this.btnSendDateTime = new System.Windows.Forms.Button();
            this.dtpDateTimeSend = new System.Windows.Forms.DateTimePicker();
            this.grpEvents = new System.Windows.Forms.GroupBox();
            this.btnClearLst = new System.Windows.Forms.Button();
            this.btnCopyList = new System.Windows.Forms.Button();
            this.lsvEvents = new System.Windows.Forms.ListView();
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colLog = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpPersonData = new System.Windows.Forms.GroupBox();
            this.cmbIndex = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.btnVarBynaryToImgAndFace = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtGrupo = new System.Windows.Forms.TextBox();
            this.chkGetAllDataPersons = new System.Windows.Forms.CheckBox();
            this.picPhotoFace = new System.Windows.Forms.PictureBox();
            this.btnFaceToImg = new System.Windows.Forms.Button();
            this.BtnOpenDoor = new System.Windows.Forms.Button();
            this.btnGetFaceByIdPerson = new System.Windows.Forms.Button();
            this.btnSendFingerPrint = new System.Windows.Forms.Button();
            this.btnSendFace = new System.Windows.Forms.Button();
            this.txtFaceToSend = new System.Windows.Forms.TextBox();
            this.lblFaceToSend = new System.Windows.Forms.Label();
            this.txtFingerPrintToSend = new System.Windows.Forms.TextBox();
            this.blFingerPrintSend = new System.Windows.Forms.Label();
            this.btnGetBiometricData = new System.Windows.Forms.Button();
            this.btnExcludePerson = new System.Windows.Forms.Button();
            this.txtCard = new System.Windows.Forms.TextBox();
            this.lblCard = new System.Windows.Forms.Label();
            this.lblValEnd = new System.Windows.Forms.Label();
            this.lblValStart = new System.Windows.Forms.Label();
            this.dtValEnd = new System.Windows.Forms.DateTimePicker();
            this.dtValStart = new System.Windows.Forms.DateTimePicker();
            this.txtPassWord = new System.Windows.Forms.TextBox();
            this.lblPassWord = new System.Windows.Forms.Label();
            this.btnGetPerson = new System.Windows.Forms.Button();
            this.btnSendPerson = new System.Windows.Forms.Button();
            this.txtNamePerson = new System.Windows.Forms.TextBox();
            this.txtIdPerson = new System.Windows.Forms.TextBox();
            this.chkSupervisor = new System.Windows.Forms.CheckBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.grpConfig = new System.Windows.Forms.GroupBox();
            this.btnSendMultiCardOn = new System.Windows.Forms.Button();
            this.chkMultiCardOn = new System.Windows.Forms.CheckBox();
            this.btnAutoServerMode = new System.Windows.Forms.Button();
            this.chkAutoServerMode = new System.Windows.Forms.CheckBox();
            this.btnSendAutoServerFun = new System.Windows.Forms.Button();
            this.chkAutoServerFun = new System.Windows.Forms.CheckBox();
            this.btnConfigGet = new System.Windows.Forms.Button();
            this.btnConfigSend = new System.Windows.Forms.Button();
            this.grpHostInfo = new System.Windows.Forms.GroupBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.txtHost = new System.Windows.Forms.TextBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblHost = new System.Windows.Forms.Label();
            this.ofdFindFile = new System.Windows.Forms.OpenFileDialog();
            this.grpDateTimeSend.SuspendLayout();
            this.grpEvents.SuspendLayout();
            this.grpPersonData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPhotoFace)).BeginInit();
            this.grpConfig.SuspendLayout();
            this.grpHostInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnWaitConnect
            // 
            this.btnWaitConnect.Location = new System.Drawing.Point(13, 4);
            this.btnWaitConnect.Name = "btnWaitConnect";
            this.btnWaitConnect.Size = new System.Drawing.Size(171, 23);
            this.btnWaitConnect.TabIndex = 0;
            this.btnWaitConnect.Text = "Aguardar Conexão";
            this.btnWaitConnect.UseVisualStyleBackColor = true;
            this.btnWaitConnect.Click += new System.EventHandler(this.btnWaitConnect_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblStatus.ForeColor = System.Drawing.Color.Maroon;
            this.lblStatus.Location = new System.Drawing.Point(490, 7);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(496, 33);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpDateTimeSend
            // 
            this.grpDateTimeSend.Controls.Add(this.btnGetDateTime);
            this.grpDateTimeSend.Controls.Add(this.btnSendDateTime);
            this.grpDateTimeSend.Controls.Add(this.dtpDateTimeSend);
            this.grpDateTimeSend.Location = new System.Drawing.Point(6, 35);
            this.grpDateTimeSend.Name = "grpDateTimeSend";
            this.grpDateTimeSend.Size = new System.Drawing.Size(190, 101);
            this.grpDateTimeSend.TabIndex = 2;
            this.grpDateTimeSend.TabStop = false;
            this.grpDateTimeSend.Text = "Data e Hora";
            // 
            // btnGetDateTime
            // 
            this.btnGetDateTime.Location = new System.Drawing.Point(15, 72);
            this.btnGetDateTime.Name = "btnGetDateTime";
            this.btnGetDateTime.Size = new System.Drawing.Size(163, 23);
            this.btnGetDateTime.TabIndex = 2;
            this.btnGetDateTime.Text = "Obter";
            this.btnGetDateTime.UseVisualStyleBackColor = true;
            this.btnGetDateTime.Click += new System.EventHandler(this.btnGetDateTime_Click);
            // 
            // btnSendDateTime
            // 
            this.btnSendDateTime.Location = new System.Drawing.Point(15, 46);
            this.btnSendDateTime.Name = "btnSendDateTime";
            this.btnSendDateTime.Size = new System.Drawing.Size(163, 23);
            this.btnSendDateTime.TabIndex = 1;
            this.btnSendDateTime.Text = "Enviar";
            this.btnSendDateTime.UseVisualStyleBackColor = true;
            this.btnSendDateTime.Click += new System.EventHandler(this.btnSendDateTime_Click);
            // 
            // dtpDateTimeSend
            // 
            this.dtpDateTimeSend.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpDateTimeSend.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTimeSend.Location = new System.Drawing.Point(13, 21);
            this.dtpDateTimeSend.Name = "dtpDateTimeSend";
            this.dtpDateTimeSend.Size = new System.Drawing.Size(166, 20);
            this.dtpDateTimeSend.TabIndex = 0;
            // 
            // grpEvents
            // 
            this.grpEvents.Controls.Add(this.btnClearLst);
            this.grpEvents.Controls.Add(this.btnCopyList);
            this.grpEvents.Controls.Add(this.lsvEvents);
            this.grpEvents.Location = new System.Drawing.Point(13, 371);
            this.grpEvents.Name = "grpEvents";
            this.grpEvents.Size = new System.Drawing.Size(973, 181);
            this.grpEvents.TabIndex = 3;
            this.grpEvents.TabStop = false;
            this.grpEvents.Text = "Eventos";
            // 
            // btnClearLst
            // 
            this.btnClearLst.Location = new System.Drawing.Point(729, 151);
            this.btnClearLst.Name = "btnClearLst";
            this.btnClearLst.Size = new System.Drawing.Size(112, 23);
            this.btnClearLst.TabIndex = 7;
            this.btnClearLst.Text = "Limpar";
            this.btnClearLst.UseVisualStyleBackColor = true;
            this.btnClearLst.Click += new System.EventHandler(this.btnClearLst_Click);
            // 
            // btnCopyList
            // 
            this.btnCopyList.Location = new System.Drawing.Point(847, 151);
            this.btnCopyList.Name = "btnCopyList";
            this.btnCopyList.Size = new System.Drawing.Size(112, 23);
            this.btnCopyList.TabIndex = 6;
            this.btnCopyList.Text = "Copiar";
            this.btnCopyList.UseVisualStyleBackColor = true;
            this.btnCopyList.Click += new System.EventHandler(this.btnCopyList_Click);
            // 
            // lsvEvents
            // 
            this.lsvEvents.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colTime,
            this.colLog});
            this.lsvEvents.GridLines = true;
            this.lsvEvents.HideSelection = false;
            this.lsvEvents.Location = new System.Drawing.Point(12, 20);
            this.lsvEvents.Name = "lsvEvents";
            this.lsvEvents.Size = new System.Drawing.Size(947, 125);
            this.lsvEvents.TabIndex = 0;
            this.lsvEvents.UseCompatibleStateImageBehavior = false;
            this.lsvEvents.View = System.Windows.Forms.View.Details;
            // 
            // colTime
            // 
            this.colTime.Text = "DateTime";
            this.colTime.Width = 180;
            // 
            // colLog
            // 
            this.colLog.Text = "Log";
            this.colLog.Width = 800;
            // 
            // grpPersonData
            // 
            this.grpPersonData.Controls.Add(this.cmbIndex);
            this.grpPersonData.Controls.Add(this.label3);
            this.grpPersonData.Controls.Add(this.label2);
            this.grpPersonData.Controls.Add(this.cmbType);
            this.grpPersonData.Controls.Add(this.btnVarBynaryToImgAndFace);
            this.grpPersonData.Controls.Add(this.label1);
            this.grpPersonData.Controls.Add(this.txtGrupo);
            this.grpPersonData.Controls.Add(this.chkGetAllDataPersons);
            this.grpPersonData.Controls.Add(this.picPhotoFace);
            this.grpPersonData.Controls.Add(this.btnFaceToImg);
            this.grpPersonData.Controls.Add(this.BtnOpenDoor);
            this.grpPersonData.Controls.Add(this.btnGetFaceByIdPerson);
            this.grpPersonData.Controls.Add(this.btnSendFingerPrint);
            this.grpPersonData.Controls.Add(this.btnSendFace);
            this.grpPersonData.Controls.Add(this.txtFaceToSend);
            this.grpPersonData.Controls.Add(this.lblFaceToSend);
            this.grpPersonData.Controls.Add(this.txtFingerPrintToSend);
            this.grpPersonData.Controls.Add(this.blFingerPrintSend);
            this.grpPersonData.Controls.Add(this.btnGetBiometricData);
            this.grpPersonData.Controls.Add(this.btnExcludePerson);
            this.grpPersonData.Controls.Add(this.txtCard);
            this.grpPersonData.Controls.Add(this.lblCard);
            this.grpPersonData.Controls.Add(this.lblValEnd);
            this.grpPersonData.Controls.Add(this.lblValStart);
            this.grpPersonData.Controls.Add(this.dtValEnd);
            this.grpPersonData.Controls.Add(this.dtValStart);
            this.grpPersonData.Controls.Add(this.txtPassWord);
            this.grpPersonData.Controls.Add(this.lblPassWord);
            this.grpPersonData.Controls.Add(this.btnGetPerson);
            this.grpPersonData.Controls.Add(this.btnSendPerson);
            this.grpPersonData.Controls.Add(this.txtNamePerson);
            this.grpPersonData.Controls.Add(this.txtIdPerson);
            this.grpPersonData.Controls.Add(this.chkSupervisor);
            this.grpPersonData.Controls.Add(this.lblName);
            this.grpPersonData.Controls.Add(this.lblId);
            this.grpPersonData.Location = new System.Drawing.Point(212, 44);
            this.grpPersonData.Name = "grpPersonData";
            this.grpPersonData.Size = new System.Drawing.Size(774, 321);
            this.grpPersonData.TabIndex = 4;
            this.grpPersonData.TabStop = false;
            this.grpPersonData.Text = "Pessoa";
            // 
            // cmbIndex
            // 
            this.cmbIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIndex.FormattingEnabled = true;
            this.cmbIndex.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cmbIndex.Location = new System.Drawing.Point(428, 169);
            this.cmbIndex.Name = "cmbIndex";
            this.cmbIndex.Size = new System.Drawing.Size(42, 21);
            this.cmbIndex.TabIndex = 34;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(509, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Tipo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(386, 172);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Index:";
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "0-General ",
            "1-Fingerprint ",
            "2-Face ",
            "3-VoicePrint",
            "4-Iris",
            "5-Retina",
            "6-Palm Print  ",
            "7-Finger Vein",
            "8-Palm ",
            "9-Visible Light Face "});
            this.cmbType.Location = new System.Drawing.Point(544, 169);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(96, 21);
            this.cmbType.TabIndex = 31;
            // 
            // btnVarBynaryToImgAndFace
            // 
            this.btnVarBynaryToImgAndFace.Location = new System.Drawing.Point(240, 38);
            this.btnVarBynaryToImgAndFace.Name = "btnVarBynaryToImgAndFace";
            this.btnVarBynaryToImgAndFace.Size = new System.Drawing.Size(59, 23);
            this.btnVarBynaryToImgAndFace.TabIndex = 30;
            this.btnVarBynaryToImgAndFace.Text = "ToVarBin";
            this.btnVarBynaryToImgAndFace.UseVisualStyleBackColor = true;
            this.btnVarBynaryToImgAndFace.Click += new System.EventHandler(this.btnVarBynaryToImgAndFace_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Grupo:";
            // 
            // txtGrupo
            // 
            this.txtGrupo.Location = new System.Drawing.Point(65, 166);
            this.txtGrupo.Name = "txtGrupo";
            this.txtGrupo.Size = new System.Drawing.Size(99, 20);
            this.txtGrupo.TabIndex = 28;
            this.txtGrupo.Text = "1";
            // 
            // chkGetAllDataPersons
            // 
            this.chkGetAllDataPersons.AutoSize = true;
            this.chkGetAllDataPersons.Location = new System.Drawing.Point(130, 208);
            this.chkGetAllDataPersons.Name = "chkGetAllDataPersons";
            this.chkGetAllDataPersons.Size = new System.Drawing.Size(87, 17);
            this.chkGetAllDataPersons.TabIndex = 27;
            this.chkGetAllDataPersons.Text = "Obter todas?";
            this.chkGetAllDataPersons.UseVisualStyleBackColor = true;
            // 
            // picPhotoFace
            // 
            this.picPhotoFace.Location = new System.Drawing.Point(261, 64);
            this.picPhotoFace.Name = "picPhotoFace";
            this.picPhotoFace.Size = new System.Drawing.Size(120, 140);
            this.picPhotoFace.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPhotoFace.TabIndex = 26;
            this.picPhotoFace.TabStop = false;
            // 
            // btnFaceToImg
            // 
            this.btnFaceToImg.Location = new System.Drawing.Point(300, 38);
            this.btnFaceToImg.Name = "btnFaceToImg";
            this.btnFaceToImg.Size = new System.Drawing.Size(81, 23);
            this.btnFaceToImg.TabIndex = 25;
            this.btnFaceToImg.Text = "FaceToImg";
            this.btnFaceToImg.UseVisualStyleBackColor = true;
            this.btnFaceToImg.Click += new System.EventHandler(this.btnFaceToImg_Click);
            // 
            // BtnOpenDoor
            // 
            this.BtnOpenDoor.Location = new System.Drawing.Point(12, 202);
            this.BtnOpenDoor.Name = "BtnOpenDoor";
            this.BtnOpenDoor.Size = new System.Drawing.Size(112, 23);
            this.BtnOpenDoor.TabIndex = 24;
            this.BtnOpenDoor.Text = "Liberar Acesso";
            this.BtnOpenDoor.UseVisualStyleBackColor = true;
            this.BtnOpenDoor.Click += new System.EventHandler(this.BtnOpenDoor_Click);
            // 
            // btnGetFaceByIdPerson
            // 
            this.btnGetFaceByIdPerson.Location = new System.Drawing.Point(482, 287);
            this.btnGetFaceByIdPerson.Name = "btnGetFaceByIdPerson";
            this.btnGetFaceByIdPerson.Size = new System.Drawing.Size(114, 23);
            this.btnGetFaceByIdPerson.TabIndex = 23;
            this.btnGetFaceByIdPerson.Text = "Obter Face";
            this.btnGetFaceByIdPerson.UseVisualStyleBackColor = true;
            this.btnGetFaceByIdPerson.Click += new System.EventHandler(this.btnGetFaceByIdPerson_Click);
            // 
            // btnSendFingerPrint
            // 
            this.btnSendFingerPrint.Location = new System.Drawing.Point(646, 169);
            this.btnSendFingerPrint.Name = "btnSendFingerPrint";
            this.btnSendFingerPrint.Size = new System.Drawing.Size(114, 23);
            this.btnSendFingerPrint.TabIndex = 22;
            this.btnSendFingerPrint.Text = "Enviar Digital";
            this.btnSendFingerPrint.UseVisualStyleBackColor = true;
            this.btnSendFingerPrint.Click += new System.EventHandler(this.btnSendFingerPrint_Click);
            // 
            // btnSendFace
            // 
            this.btnSendFace.Location = new System.Drawing.Point(646, 287);
            this.btnSendFace.Name = "btnSendFace";
            this.btnSendFace.Size = new System.Drawing.Size(114, 23);
            this.btnSendFace.TabIndex = 21;
            this.btnSendFace.Text = "Enviar Face";
            this.btnSendFace.UseVisualStyleBackColor = true;
            this.btnSendFace.Click += new System.EventHandler(this.btnSendFace_Click);
            // 
            // txtFaceToSend
            // 
            this.txtFaceToSend.Location = new System.Drawing.Point(385, 209);
            this.txtFaceToSend.MaxLength = 1000000;
            this.txtFaceToSend.Multiline = true;
            this.txtFaceToSend.Name = "txtFaceToSend";
            this.txtFaceToSend.Size = new System.Drawing.Size(375, 73);
            this.txtFaceToSend.TabIndex = 20;
            // 
            // lblFaceToSend
            // 
            this.lblFaceToSend.AutoSize = true;
            this.lblFaceToSend.Location = new System.Drawing.Point(382, 191);
            this.lblFaceToSend.Name = "lblFaceToSend";
            this.lblFaceToSend.Size = new System.Drawing.Size(88, 13);
            this.lblFaceToSend.TabIndex = 19;
            this.lblFaceToSend.Text = "Face para Envio:";
            // 
            // txtFingerPrintToSend
            // 
            this.txtFingerPrintToSend.Location = new System.Drawing.Point(385, 40);
            this.txtFingerPrintToSend.Multiline = true;
            this.txtFingerPrintToSend.Name = "txtFingerPrintToSend";
            this.txtFingerPrintToSend.Size = new System.Drawing.Size(375, 121);
            this.txtFingerPrintToSend.TabIndex = 18;
            // 
            // blFingerPrintSend
            // 
            this.blFingerPrintSend.AutoSize = true;
            this.blFingerPrintSend.Location = new System.Drawing.Point(382, 24);
            this.blFingerPrintSend.Name = "blFingerPrintSend";
            this.blFingerPrintSend.Size = new System.Drawing.Size(93, 13);
            this.blFingerPrintSend.TabIndex = 17;
            this.blFingerPrintSend.Text = "Digital para Envio:";
            // 
            // btnGetBiometricData
            // 
            this.btnGetBiometricData.Location = new System.Drawing.Point(362, 287);
            this.btnGetBiometricData.Name = "btnGetBiometricData";
            this.btnGetBiometricData.Size = new System.Drawing.Size(114, 23);
            this.btnGetBiometricData.TabIndex = 16;
            this.btnGetBiometricData.Text = "Obter Biometria";
            this.btnGetBiometricData.UseVisualStyleBackColor = true;
            this.btnGetBiometricData.Click += new System.EventHandler(this.btnGetBiometricData_Click);
            // 
            // btnExcludePerson
            // 
            this.btnExcludePerson.Location = new System.Drawing.Point(244, 228);
            this.btnExcludePerson.Name = "btnExcludePerson";
            this.btnExcludePerson.Size = new System.Drawing.Size(112, 23);
            this.btnExcludePerson.TabIndex = 15;
            this.btnExcludePerson.Text = "Excluir";
            this.btnExcludePerson.UseVisualStyleBackColor = true;
            this.btnExcludePerson.Click += new System.EventHandler(this.btnExcludePerson_Click);
            // 
            // txtCard
            // 
            this.txtCard.Location = new System.Drawing.Point(65, 93);
            this.txtCard.Name = "txtCard";
            this.txtCard.Size = new System.Drawing.Size(173, 20);
            this.txtCard.TabIndex = 14;
            this.txtCard.Text = "0";
            // 
            // lblCard
            // 
            this.lblCard.AutoSize = true;
            this.lblCard.Location = new System.Drawing.Point(25, 96);
            this.lblCard.Name = "lblCard";
            this.lblCard.Size = new System.Drawing.Size(41, 13);
            this.lblCard.TabIndex = 13;
            this.lblCard.Text = "Cartão:";
            // 
            // lblValEnd
            // 
            this.lblValEnd.AutoSize = true;
            this.lblValEnd.Location = new System.Drawing.Point(37, 144);
            this.lblValEnd.Name = "lblValEnd";
            this.lblValEnd.Size = new System.Drawing.Size(26, 13);
            this.lblValEnd.TabIndex = 12;
            this.lblValEnd.Text = "Até:";
            // 
            // lblValStart
            // 
            this.lblValStart.AutoSize = true;
            this.lblValStart.Location = new System.Drawing.Point(39, 119);
            this.lblValStart.Name = "lblValStart";
            this.lblValStart.Size = new System.Drawing.Size(24, 13);
            this.lblValStart.TabIndex = 11;
            this.lblValStart.Text = "De:";
            // 
            // dtValEnd
            // 
            this.dtValEnd.CustomFormat = "dd/MM/yyyy";
            this.dtValEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtValEnd.Location = new System.Drawing.Point(65, 141);
            this.dtValEnd.Name = "dtValEnd";
            this.dtValEnd.Size = new System.Drawing.Size(99, 20);
            this.dtValEnd.TabIndex = 10;
            // 
            // dtValStart
            // 
            this.dtValStart.CustomFormat = "dd/MM/yyyy";
            this.dtValStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtValStart.Location = new System.Drawing.Point(65, 115);
            this.dtValStart.Name = "dtValStart";
            this.dtValStart.Size = new System.Drawing.Size(99, 20);
            this.dtValStart.TabIndex = 9;
            // 
            // txtPassWord
            // 
            this.txtPassWord.Location = new System.Drawing.Point(65, 71);
            this.txtPassWord.Name = "txtPassWord";
            this.txtPassWord.Size = new System.Drawing.Size(173, 20);
            this.txtPassWord.TabIndex = 8;
            // 
            // lblPassWord
            // 
            this.lblPassWord.AutoSize = true;
            this.lblPassWord.Location = new System.Drawing.Point(25, 74);
            this.lblPassWord.Name = "lblPassWord";
            this.lblPassWord.Size = new System.Drawing.Size(41, 13);
            this.lblPassWord.TabIndex = 7;
            this.lblPassWord.Text = "Senha:";
            // 
            // btnGetPerson
            // 
            this.btnGetPerson.Location = new System.Drawing.Point(130, 231);
            this.btnGetPerson.Name = "btnGetPerson";
            this.btnGetPerson.Size = new System.Drawing.Size(97, 23);
            this.btnGetPerson.TabIndex = 6;
            this.btnGetPerson.Text = "Obter";
            this.btnGetPerson.UseVisualStyleBackColor = true;
            this.btnGetPerson.Click += new System.EventHandler(this.btnGetPerson_Click);
            // 
            // btnSendPerson
            // 
            this.btnSendPerson.Location = new System.Drawing.Point(12, 231);
            this.btnSendPerson.Name = "btnSendPerson";
            this.btnSendPerson.Size = new System.Drawing.Size(112, 23);
            this.btnSendPerson.TabIndex = 5;
            this.btnSendPerson.Text = "Enviar";
            this.btnSendPerson.UseVisualStyleBackColor = true;
            this.btnSendPerson.Click += new System.EventHandler(this.btnSendPerson_Click);
            // 
            // txtNamePerson
            // 
            this.txtNamePerson.Location = new System.Drawing.Point(65, 45);
            this.txtNamePerson.Name = "txtNamePerson";
            this.txtNamePerson.Size = new System.Drawing.Size(162, 20);
            this.txtNamePerson.TabIndex = 4;
            // 
            // txtIdPerson
            // 
            this.txtIdPerson.Location = new System.Drawing.Point(65, 21);
            this.txtIdPerson.Name = "txtIdPerson";
            this.txtIdPerson.Size = new System.Drawing.Size(162, 20);
            this.txtIdPerson.TabIndex = 3;
            // 
            // chkSupervisor
            // 
            this.chkSupervisor.AutoSize = true;
            this.chkSupervisor.Location = new System.Drawing.Point(233, 23);
            this.chkSupervisor.Name = "chkSupervisor";
            this.chkSupervisor.Size = new System.Drawing.Size(85, 17);
            this.chkSupervisor.TabIndex = 2;
            this.chkSupervisor.Text = "Supervisor ?";
            this.chkSupervisor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkSupervisor.UseVisualStyleBackColor = true;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(25, 48);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Nome:";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(25, 24);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(22, 13);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "Id.:";
            // 
            // grpConfig
            // 
            this.grpConfig.Controls.Add(this.btnSendMultiCardOn);
            this.grpConfig.Controls.Add(this.chkMultiCardOn);
            this.grpConfig.Controls.Add(this.btnAutoServerMode);
            this.grpConfig.Controls.Add(this.chkAutoServerMode);
            this.grpConfig.Controls.Add(this.btnSendAutoServerFun);
            this.grpConfig.Controls.Add(this.chkAutoServerFun);
            this.grpConfig.Controls.Add(this.btnConfigGet);
            this.grpConfig.Controls.Add(this.btnConfigSend);
            this.grpConfig.Location = new System.Drawing.Point(7, 141);
            this.grpConfig.Name = "grpConfig";
            this.grpConfig.Size = new System.Drawing.Size(188, 224);
            this.grpConfig.TabIndex = 5;
            this.grpConfig.TabStop = false;
            this.grpConfig.Text = "Configurações";
            // 
            // btnSendMultiCardOn
            // 
            this.btnSendMultiCardOn.Location = new System.Drawing.Point(121, 136);
            this.btnSendMultiCardOn.Name = "btnSendMultiCardOn";
            this.btnSendMultiCardOn.Size = new System.Drawing.Size(56, 23);
            this.btnSendMultiCardOn.TabIndex = 9;
            this.btnSendMultiCardOn.Text = "Enviar";
            this.btnSendMultiCardOn.UseVisualStyleBackColor = true;
            this.btnSendMultiCardOn.Click += new System.EventHandler(this.btnSendMultiCardOn_Click);
            // 
            // chkMultiCardOn
            // 
            this.chkMultiCardOn.AutoSize = true;
            this.chkMultiCardOn.Location = new System.Drawing.Point(18, 140);
            this.chkMultiCardOn.Name = "chkMultiCardOn";
            this.chkMultiCardOn.Size = new System.Drawing.Size(84, 17);
            this.chkMultiCardOn.TabIndex = 8;
            this.chkMultiCardOn.Text = "MultiCardOn";
            this.chkMultiCardOn.UseVisualStyleBackColor = true;
            // 
            // btnAutoServerMode
            // 
            this.btnAutoServerMode.Location = new System.Drawing.Point(121, 109);
            this.btnAutoServerMode.Name = "btnAutoServerMode";
            this.btnAutoServerMode.Size = new System.Drawing.Size(56, 23);
            this.btnAutoServerMode.TabIndex = 7;
            this.btnAutoServerMode.Text = "Enviar";
            this.btnAutoServerMode.UseVisualStyleBackColor = true;
            this.btnAutoServerMode.Click += new System.EventHandler(this.btnAutoServerMode_Click);
            // 
            // chkAutoServerMode
            // 
            this.chkAutoServerMode.AutoSize = true;
            this.chkAutoServerMode.Location = new System.Drawing.Point(18, 113);
            this.chkAutoServerMode.Name = "chkAutoServerMode";
            this.chkAutoServerMode.Size = new System.Drawing.Size(106, 17);
            this.chkAutoServerMode.TabIndex = 6;
            this.chkAutoServerMode.Text = "AutoServerMode";
            this.chkAutoServerMode.UseVisualStyleBackColor = true;
            // 
            // btnSendAutoServerFun
            // 
            this.btnSendAutoServerFun.Location = new System.Drawing.Point(121, 79);
            this.btnSendAutoServerFun.Name = "btnSendAutoServerFun";
            this.btnSendAutoServerFun.Size = new System.Drawing.Size(56, 23);
            this.btnSendAutoServerFun.TabIndex = 5;
            this.btnSendAutoServerFun.Text = "Enviar";
            this.btnSendAutoServerFun.UseVisualStyleBackColor = true;
            this.btnSendAutoServerFun.Click += new System.EventHandler(this.btnSendAutoServerFun_Click);
            // 
            // chkAutoServerFun
            // 
            this.chkAutoServerFun.AutoSize = true;
            this.chkAutoServerFun.Location = new System.Drawing.Point(18, 83);
            this.chkAutoServerFun.Name = "chkAutoServerFun";
            this.chkAutoServerFun.Size = new System.Drawing.Size(97, 17);
            this.chkAutoServerFun.TabIndex = 4;
            this.chkAutoServerFun.Text = "AutoServerFun";
            this.chkAutoServerFun.UseVisualStyleBackColor = true;
            // 
            // btnConfigGet
            // 
            this.btnConfigGet.Location = new System.Drawing.Point(14, 48);
            this.btnConfigGet.Name = "btnConfigGet";
            this.btnConfigGet.Size = new System.Drawing.Size(163, 23);
            this.btnConfigGet.TabIndex = 3;
            this.btnConfigGet.Text = "Obter";
            this.btnConfigGet.UseVisualStyleBackColor = true;
            this.btnConfigGet.Click += new System.EventHandler(this.btnConfigGet_Click);
            // 
            // btnConfigSend
            // 
            this.btnConfigSend.Location = new System.Drawing.Point(14, 22);
            this.btnConfigSend.Name = "btnConfigSend";
            this.btnConfigSend.Size = new System.Drawing.Size(163, 23);
            this.btnConfigSend.TabIndex = 2;
            this.btnConfigSend.Text = "Enviar";
            this.btnConfigSend.UseVisualStyleBackColor = true;
            this.btnConfigSend.Click += new System.EventHandler(this.btnConfigSend_Click);
            // 
            // grpHostInfo
            // 
            this.grpHostInfo.Controls.Add(this.txtPort);
            this.grpHostInfo.Controls.Add(this.txtHost);
            this.grpHostInfo.Controls.Add(this.lblPort);
            this.grpHostInfo.Controls.Add(this.lblHost);
            this.grpHostInfo.Location = new System.Drawing.Point(212, 0);
            this.grpHostInfo.Name = "grpHostInfo";
            this.grpHostInfo.Size = new System.Drawing.Size(271, 43);
            this.grpHostInfo.TabIndex = 6;
            this.grpHostInfo.TabStop = false;
            this.grpHostInfo.Text = "Host Info";
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(214, 13);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(46, 20);
            this.txtPort.TabIndex = 3;
            this.txtPort.Text = "8090";
            // 
            // txtHost
            // 
            this.txtHost.Location = new System.Drawing.Point(45, 13);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(126, 20);
            this.txtHost.TabIndex = 2;
            this.txtHost.Text = "192.168.0.102";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(178, 17);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(35, 13);
            this.lblPort.TabIndex = 1;
            this.lblPort.Text = "Porta:";
            // 
            // lblHost
            // 
            this.lblHost.AutoSize = true;
            this.lblHost.Location = new System.Drawing.Point(11, 17);
            this.lblHost.Name = "lblHost";
            this.lblHost.Size = new System.Drawing.Size(32, 13);
            this.lblHost.TabIndex = 0;
            this.lblHost.Text = "Host:";
            // 
            // ofdFindFile
            // 
            this.ofdFindFile.FileName = "face.jpg";
            // 
            // frmZKPushApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 561);
            this.Controls.Add(this.grpHostInfo);
            this.Controls.Add(this.grpConfig);
            this.Controls.Add(this.grpPersonData);
            this.Controls.Add(this.grpEvents);
            this.Controls.Add(this.grpDateTimeSend);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnWaitConnect);
            this.Name = "frmZKPushApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Comunicação ZK Push Protocol";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpDateTimeSend.ResumeLayout(false);
            this.grpEvents.ResumeLayout(false);
            this.grpPersonData.ResumeLayout(false);
            this.grpPersonData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picPhotoFace)).EndInit();
            this.grpConfig.ResumeLayout(false);
            this.grpConfig.PerformLayout();
            this.grpHostInfo.ResumeLayout(false);
            this.grpHostInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWaitConnect;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.GroupBox grpDateTimeSend;
        private System.Windows.Forms.Button btnSendDateTime;
        private System.Windows.Forms.DateTimePicker dtpDateTimeSend;
        private System.Windows.Forms.GroupBox grpEvents;
        private System.Windows.Forms.ListView lsvEvents;
        private System.Windows.Forms.ColumnHeader colTime;
        private System.Windows.Forms.ColumnHeader colLog;
        private System.Windows.Forms.GroupBox grpPersonData;
        private System.Windows.Forms.Button btnGetPerson;
        private System.Windows.Forms.Button btnSendPerson;
        private System.Windows.Forms.TextBox txtNamePerson;
        private System.Windows.Forms.TextBox txtIdPerson;
        private System.Windows.Forms.CheckBox chkSupervisor;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblValEnd;
        private System.Windows.Forms.Label lblValStart;
        private System.Windows.Forms.DateTimePicker dtValEnd;
        private System.Windows.Forms.DateTimePicker dtValStart;
        private System.Windows.Forms.TextBox txtPassWord;
        private System.Windows.Forms.Label lblPassWord;
        private System.Windows.Forms.TextBox txtCard;
        private System.Windows.Forms.Label lblCard;
        private System.Windows.Forms.Button btnExcludePerson;
        private System.Windows.Forms.Button btnGetBiometricData;
        private System.Windows.Forms.Button btnClearLst;
        private System.Windows.Forms.Button btnCopyList;
        private System.Windows.Forms.Button btnSendFace;
        private System.Windows.Forms.TextBox txtFaceToSend;
        private System.Windows.Forms.Label lblFaceToSend;
        private System.Windows.Forms.TextBox txtFingerPrintToSend;
        private System.Windows.Forms.Label blFingerPrintSend;
        private System.Windows.Forms.Button btnSendFingerPrint;
        private System.Windows.Forms.GroupBox grpConfig;
        private System.Windows.Forms.Button btnConfigGet;
        private System.Windows.Forms.Button btnConfigSend;
        private System.Windows.Forms.Button btnGetFaceByIdPerson;
        private System.Windows.Forms.Button btnGetDateTime;
        private System.Windows.Forms.Button BtnOpenDoor;
        private System.Windows.Forms.Button btnSendAutoServerFun;
        private System.Windows.Forms.CheckBox chkAutoServerFun;
        private System.Windows.Forms.GroupBox grpHostInfo;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.TextBox txtHost;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblHost;
        private System.Windows.Forms.Button btnFaceToImg;
        private System.Windows.Forms.OpenFileDialog ofdFindFile;
        private System.Windows.Forms.PictureBox picPhotoFace;
        private System.Windows.Forms.Button btnAutoServerMode;
        private System.Windows.Forms.CheckBox chkAutoServerMode;
        private System.Windows.Forms.CheckBox chkGetAllDataPersons;
        private System.Windows.Forms.Button btnSendMultiCardOn;
        private System.Windows.Forms.CheckBox chkMultiCardOn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtGrupo;
        private System.Windows.Forms.Button btnVarBynaryToImgAndFace;
        private System.Windows.Forms.ComboBox cmbIndex;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbType;
    }
}

